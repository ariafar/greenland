/*!
 * jQuery Transit - CSS3 transitions and transformations
 * Copyright(c) 2011 Rico Sta. Cruz <rico@ricostacruz.com>
 * MIT Licensed.
 *
 * http://ricostacruz.com/jquery.transit
 * http://github.com/rstacruz/jquery.transit
 */
 
 
 
 
 /*!
	jQuery WaitForImages
  
	Copyright (c) 2012 Alex Dickson

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the
	Software is furnished to do so, subject to the following
	conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.


	https://github.com/alexanderdickson/waitForImages
	
	
 */
 
 // WAIT FOR IMAGES
	/*
	* waitForImages 1.4
	* -----------------
	* Provides a callback when all images have loaded in your given selector.
	* http://www.alexanderdickson.com/
	*
	*
	* Copyright (c) 2011 Alex Dickson
	* Licensed under the MIT licenses.
	* See website for more info.
	*
	*/
	

	
// EASINGS
 
 
 
 
(function ($, undefined) {
    (function(a){function d(a){var c=["Moz","Webkit","O","ms"];var d=a.charAt(0).toUpperCase()+a.substr(1);if(a in b.style){return a}for(var e=0;e<c.length;++e){var f=c[e]+d;if(f in b.style){return f}}}function e(){b.style[c.transform]="";b.style[c.transform]="rotateY(90deg)";return b.style[c.transform]!==""}function i(a){if(typeof a==="string"){this.parse(a)}return this}function j(a,b,c){if(b===true){a.queue(c)}else if(b){a.queue(b,c)}else{c()}}function k(b){var c=[];a.each(b,function(b){b=a.camelCase(b);b=a.transit.propertyMap[b]||b;b=n(b);if(a.inArray(b,c)===-1){c.push(b)}});return c}function l(b,c,d,e){var f=k(b);if(a.cssEase[d]){d=a.cssEase[d]}var g=""+p(c)+" "+d;if(parseInt(e,10)>0){g+=" "+p(e)}var h=[];a.each(f,function(a,b){h.push(b+" "+g)});return h.join(", ")}function m(b,d){if(!d){a.cssNumber[b]=true}a.transit.propertyMap[b]=c.transform;a.cssHooks[b]={get:function(c){var d=a(c).css("transform");return d.get(b)},set:function(c,d){var e=a(c).css("transform");e.setFromString(b,d);a(c).css({transform:e})}}}function n(a){return a.replace(/([A-Z])/g,function(a){return"-"+a.toLowerCase()})}function o(a,b){if(typeof a==="string"&&!a.match(/^[\-0-9\.]+$/)){return a}else{return""+a+b}}function p(b){var c=b;if(a.fx.speeds[c]){c=a.fx.speeds[c]}return o(c,"ms")}"use strict";a.transit={version:"0.1.3",propertyMap:{marginLeft:"margin",marginRight:"margin",marginBottom:"margin",marginTop:"margin",paddingLeft:"padding",paddingRight:"padding",paddingBottom:"padding",paddingTop:"padding"},enabled:true,useTransitionEnd:false};var b=document.createElement("div");var c={};var f=navigator.userAgent.toLowerCase().indexOf("chrome")>-1;c.transition=d("transition");c.transitionDelay=d("transitionDelay");c.transform=d("transform");c.transformOrigin=d("transformOrigin");c.transform3d=e();a.extend(a.support,c);var g={MozTransition:"transitionend",OTransition:"oTransitionEnd",WebkitTransition:"webkitTransitionEnd",msTransition:"MSTransitionEnd"};var h=c.transitionEnd=g[c.transition]||null;b=null;a.cssEase={_default:"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)"};a.cssHooks.transform={get:function(b){return a(b).data("transform")||new i},set:function(b,d){var e=d;if(!(e instanceof i)){e=new i(e)}if(c.transform==="WebkitTransform"&&!f){b.style[c.transform]=e.toString(true)}else{b.style[c.transform]=e.toString()}a(b).data("transform",e)}};a.cssHooks.transformOrigin={get:function(a){return a.style[c.transformOrigin]},set:function(a,b){a.style[c.transformOrigin]=b}};a.cssHooks.transition={get:function(a){return a.style[c.transition]},set:function(a,b){a.style[c.transition]=b}};m("scale");m("translate");m("rotate");m("rotateX");m("rotateY");m("rotate3d");m("perspective");m("skewX");m("skewY");m("x",true);m("y",true);i.prototype={setFromString:function(a,b){var c=typeof b==="string"?b.split(","):b.constructor===Array?b:[b];c.unshift(a);i.prototype.set.apply(this,c)},set:function(a){var b=Array.prototype.slice.apply(arguments,[1]);if(this.setter[a]){this.setter[a].apply(this,b)}else{this[a]=b.join(",")}},get:function(a){if(this.getter[a]){return this.getter[a].apply(this)}else{return this[a]||0}},setter:{rotate:function(a){this.rotate=o(a,"deg")},rotateX:function(a){this.rotateX=o(a,"deg")},rotateY:function(a){this.rotateY=o(a,"deg")},scale:function(a,b){if(b===undefined){b=a}this.scale=a+","+b},skewX:function(a){this.skewX=o(a,"deg")},skewY:function(a){this.skewY=o(a,"deg")},perspective:function(a){this.perspective=o(a,"px")},x:function(a){this.set("translate",a,null)},y:function(a){this.set("translate",null,a)},translate:function(a,b){if(this._translateX===undefined){this._translateX=0}if(this._translateY===undefined){this._translateY=0}if(a!==null){this._translateX=o(a,"px")}if(b!==null){this._translateY=o(b,"px")}this.translate=this._translateX+","+this._translateY}},getter:{x:function(){return this._translateX||0},y:function(){return this._translateY||0},scale:function(){var a=(this.scale||"1,1").split(",");if(a[0]){a[0]=parseFloat(a[0])}if(a[1]){a[1]=parseFloat(a[1])}return a[0]===a[1]?a[0]:a},rotate3d:function(){var a=(this.rotate3d||"0,0,0,0deg").split(",");for(var b=0;b<=3;++b){if(a[b]){a[b]=parseFloat(a[b])}}if(a[3]){a[3]=o(a[3],"deg")}return a}},parse:function(a){var b=this;a.replace(/([a-zA-Z0-9]+)\((.*?)\)/g,function(a,c,d){b.setFromString(c,d)})},toString:function(a){var b=[];for(var d in this){if(this.hasOwnProperty(d)){if(!c.transform3d&&(d==="rotateX"||d==="rotateY"||d==="perspective"||d==="transformOrigin")){continue}if(d[0]!=="_"){if(a&&d==="scale"){b.push(d+"3d("+this[d]+",1)")}else if(a&&d==="translate"){b.push(d+"3d("+this[d]+",0)")}else{b.push(d+"("+this[d]+")")}}}}return b.join(" ")}};a.fn.transition=a.fn.transit=function(b,d,e,f){var g=this;var i=0;var k=true;if(typeof d==="function"){f=d;d=undefined}if(typeof e==="function"){f=e;e=undefined}if(typeof b.easing!=="undefined"){e=b.easing;delete b.easing}if(typeof b.duration!=="undefined"){d=b.duration;delete b.duration}if(typeof b.complete!=="undefined"){f=b.complete;delete b.complete}if(typeof b.queue!=="undefined"){k=b.queue;delete b.queue}if(typeof b.delay!=="undefined"){i=b.delay;delete b.delay}if(typeof d==="undefined"){d=a.fx.speeds._default}if(typeof e==="undefined"){e=a.cssEase._default}d=p(d);var m=l(b,d,e,i);var n=a.transit.enabled&&c.transition;var o=n?parseInt(d,10)+parseInt(i,10):0;if(o===0){var q=function(a){g.css(b);if(f){f.apply(g)}if(a){a()}};j(g,k,q);return g}var r={};var s=function(d){var e=false;var i=function(){if(e){g.unbind(h,i)}if(o>0){g.each(function(){this.style[c.transition]=r[this]||null})}if(typeof f==="function"){f.apply(g)}if(typeof d==="function"){d()}};if(o>0&&h&&a.transit.useTransitionEnd){e=true;g.bind(h,i)}else{window.setTimeout(i,o)}g.each(function(){if(o>0){this.style[c.transition]=m}a(this).css(b)})};var t=function(a){var b=0;if(c.transition==="MozTransition"&&b<25){b=25}window.setTimeout(function(){s(a)},b)};j(g,k,t);return this};a.transit.getTransitionValue=l})(jQuery);(function(a,b){jQuery.easing["jswing"]=jQuery.easing["swing"];jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(a,b,c,d,e){return jQuery.easing[jQuery.easing.def](a,b,c,d,e)},easeInQuad:function(a,b,c,d,e){return d*(b/=e)*b+c},easeOutQuad:function(a,b,c,d,e){return-d*(b/=e)*(b-2)+c},easeInOutQuad:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b+c;return-d/2*(--b*(b-2)-1)+c},easeInCubic:function(a,b,c,d,e){return d*(b/=e)*b*b+c},easeOutCubic:function(a,b,c,d,e){return d*((b=b/e-1)*b*b+1)+c},easeInOutCubic:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b+c;return d/2*((b-=2)*b*b+2)+c},easeInQuart:function(a,b,c,d,e){return d*(b/=e)*b*b*b+c},easeOutQuart:function(a,b,c,d,e){return-d*((b=b/e-1)*b*b*b-1)+c},easeInOutQuart:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b+c;return-d/2*((b-=2)*b*b*b-2)+c},easeInQuint:function(a,b,c,d,e){return d*(b/=e)*b*b*b*b+c},easeOutQuint:function(a,b,c,d,e){return d*((b=b/e-1)*b*b*b*b+1)+c},easeInOutQuint:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b*b+c;return d/2*((b-=2)*b*b*b*b+2)+c},easeInSine:function(a,b,c,d,e){return-d*Math.cos(b/e*(Math.PI/2))+d+c},easeOutSine:function(a,b,c,d,e){return d*Math.sin(b/e*(Math.PI/2))+c},easeInOutSine:function(a,b,c,d,e){return-d/2*(Math.cos(Math.PI*b/e)-1)+c},easeInExpo:function(a,b,c,d,e){return b==0?c:d*Math.pow(2,10*(b/e-1))+c},easeOutExpo:function(a,b,c,d,e){return b==e?c+d:d*(-Math.pow(2,-10*b/e)+1)+c},easeInOutExpo:function(a,b,c,d,e){if(b==0)return c;if(b==e)return c+d;if((b/=e/2)<1)return d/2*Math.pow(2,10*(b-1))+c;return d/2*(-Math.pow(2,-10*--b)+2)+c},easeInCirc:function(a,b,c,d,e){return-d*(Math.sqrt(1-(b/=e)*b)-1)+c},easeOutCirc:function(a,b,c,d,e){return d*Math.sqrt(1-(b=b/e-1)*b)+c},easeInOutCirc:function(a,b,c,d,e){if((b/=e/2)<1)return-d/2*(Math.sqrt(1-b*b)-1)+c;return d/2*(Math.sqrt(1-(b-=2)*b)+1)+c},easeInElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return-(h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g))+c},easeOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return h*Math.pow(2,-10*b)*Math.sin((b*e-f)*2*Math.PI/g)+d+c},easeInOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e/2)==2)return c+d;if(!g)g=e*.3*1.5;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);if(b<1)return-.5*h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)+c;return h*Math.pow(2,-10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)*.5+d+c},easeInBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*(b/=e)*b*((f+1)*b-f)+c},easeOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*((b=b/e-1)*b*((f+1)*b+f)+1)+c},easeInOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;if((b/=e/2)<1)return d/2*b*b*(((f*=1.525)+1)*b-f)+c;return d/2*((b-=2)*b*(((f*=1.525)+1)*b+f)+2)+c},easeInBounce:function(a,b,c,d,e){return d-jQuery.easing.easeOutBounce(a,e-b,0,d,e)+c},easeOutBounce:function(a,b,c,d,e){if((b/=e)<1/2.75){return d*7.5625*b*b+c}else if(b<2/2.75){return d*(7.5625*(b-=1.5/2.75)*b+.75)+c}else if(b<2.5/2.75){return d*(7.5625*(b-=2.25/2.75)*b+.9375)+c}else{return d*(7.5625*(b-=2.625/2.75)*b+.984375)+c}},easeInOutBounce:function(a,b,c,d,e){if(b<e/2)return jQuery.easing.easeInBounce(a,b*2,0,d,e)*.5+c;return jQuery.easing.easeOutBounce(a,b*2-e,0,d,e)*.5+d*.5+c}});a.waitForImages={hasImageProperties:["backgroundImage","listStyleImage","borderImage","borderCornerImage"]};a.expr[":"].uncached=function(b){var c=document.createElement("img");c.src=b.src;return a(b).is('img[src!=""]')&&!c.complete};a.fn.waitForImages=function(b,c,d){if(a.isPlainObject(arguments[0])){c=b.each;d=b.waitForAll;b=b.finished}b=b||a.noop;c=c||a.noop;d=!!d;if(!a.isFunction(b)||!a.isFunction(c)){throw new TypeError("An invalid callback was supplied.")}return this.each(function(){var e=a(this),f=[];if(d){var g=a.waitForImages.hasImageProperties||[],h=/url\((['"]?)(.*?)\1\)/g;e.find("*").each(function(){var b=a(this);if(b.is("img:uncached")){f.push({src:b.attr("src"),element:b[0]})}a.each(g,function(a,c){var d=b.css(c);if(!d){return true}var e;while(e=h.exec(d)){f.push({src:e[2],element:b[0]})}})})}else{e.find("img:uncached").each(function(){f.push({src:this.src,element:this})})}var i=f.length,j=0;if(i==0){b.call(e[0])}a.each(f,function(d,f){var g=new Image;a(g).bind("load error",function(a){j++;c.call(f.element,j,i,a.type=="load");if(j==i){b.call(e[0]);return false}});g.src=f.src})})};a.fn.swipe=function(b){if(!this)return false;var c={fingers:1,threshold:75,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,click:null,triggerOnTouchEnd:true,allowPageScroll:"auto"};var d="left";var e="right";var f="up";var g="down";var h="none";var i="horizontal";var j="vertical";var k="auto";var l="start";var m="move";var n="end";var o="cancel";var p="ontouchstart"in window,q=p?"touchstart":"mousedown",r=p?"touchmove":"mousemove",s=p?"touchend":"mouseup",t="touchcancel";var u="start";if(b.allowPageScroll==undefined&&(b.swipe!=undefined||b.swipeStatus!=undefined))b.allowPageScroll=h;if(b)a.extend(c,b);return this.each(function(){function b(){var a=v();if(a<=45&&a>=0)return d;else if(a<=360&&a>=315)return d;else if(a>=135&&a<=225)return e;else if(a>45&&a<135)return g;else return f}function v(){var a=H.x-I.x;var b=I.y-H.y;var c=Math.atan2(b,a);var d=Math.round(c*180/Math.PI);if(d<0)d=360-Math.abs(d);return d}function w(){return Math.round(Math.sqrt(Math.pow(I.x-H.x,2)+Math.pow(I.y-H.y,2)))}function x(a,b){if(c.allowPageScroll==h){a.preventDefault()}else{var l=c.allowPageScroll==k;switch(b){case d:if(c.swipeLeft&&l||!l&&c.allowPageScroll!=i)a.preventDefault();break;case e:if(c.swipeRight&&l||!l&&c.allowPageScroll!=i)a.preventDefault();break;case f:if(c.swipeUp&&l||!l&&c.allowPageScroll!=j)a.preventDefault();break;case g:if(c.swipeDown&&l||!l&&c.allowPageScroll!=j)a.preventDefault();break}}}function y(a,b){if(c.swipeStatus)c.swipeStatus.call(E,a,b,direction||null,distance||0);if(b==o){if(c.click&&(G==1||!p)&&(isNaN(distance)||distance==0))c.click.call(E,a,a.target)}if(b==n){if(c.swipe){c.swipe.call(E,a,direction,distance)}switch(direction){case d:if(c.swipeLeft)c.swipeLeft.call(E,a,direction,distance);break;case e:if(c.swipeRight)c.swipeRight.call(E,a,direction,distance);break;case f:if(c.swipeUp)c.swipeUp.call(E,a,direction,distance);break;case g:if(c.swipeDown)c.swipeDown.call(E,a,direction,distance);break}}}function z(a){G=0;H.x=0;H.y=0;I.x=0;I.y=0;J.x=0;J.y=0}function A(a){a.preventDefault();distance=w();direction=b();if(c.triggerOnTouchEnd){u=n;if((G==c.fingers||!p)&&I.x!=0){if(distance>=c.threshold){y(a,u);z(a)}else{u=o;y(a,u);z(a)}}else{u=o;y(a,u);z(a)}}else if(u==m){u=o;y(a,u);z(a)}D.removeEventListener(r,B,false);D.removeEventListener(s,A,false)}function B(a){if(u==n||u==o)return;var d=p?a.touches[0]:a;I.x=d.pageX;I.y=d.pageY;direction=b();if(p){G=a.touches.length}u=m;x(a,direction);if(G==c.fingers||!p){distance=w();if(c.swipeStatus)y(a,u,direction,distance);if(!c.triggerOnTouchEnd){if(distance>=c.threshold){u=n;y(a,u);z(a)}}}else{u=o;y(a,u);z(a)}}function C(a){var b=p?a.touches[0]:a;u=l;if(p){G=a.touches.length}distance=0;direction=null;if(G==c.fingers||!p){H.x=I.x=b.pageX;H.y=I.y=b.pageY;if(c.swipeStatus)y(a,u)}else{z(a)}D.addEventListener(r,B,false);D.addEventListener(s,A,false)}var D=this;var E=a(this);var F=null;var G=0;var H={x:0,y:0};var I={x:0,y:0};var J={x:0,y:0};try{this.addEventListener(q,C,false);this.addEventListener(t,z)}catch(K){}})}})(jQuery)
    
    $.fn.extend({
        revolution: function (options) {
            var defaults = {
                delay: 9E3,
                startheight: 490,
                startwidth: 890,
                hideThumbs: 200,
                thumbWidth: 100,
                thumbHeight: 50,
                thumbAmount: 5,
                navigationType: "both",
                navigationArrows: "nexttobullets",
                navigationStyle: "round",
                touchenabled: "on",
                onHoverStop: "on",
                navOffsetHorizontal: 0,
                navOffsetVertical: 20,
                stopAtSlide: 4,
                stopAfterLoops: 1,
                shadow: 1,
                stopLoop: "off"
            };
            options = $.extend({}, $.fn.revolution.defaults, options);
            return this.each(function () {
                var opt = options;
                var container = $(this);
                var version = $.fn.jquery.split("."),
                    versionTop = parseFloat(version[0]),
                    versionMinor = parseFloat(version[1]),
                    versionIncrement = parseFloat(version[2] || "0");
                if (versionTop == 1 && versionMinor < 7) container.html('<div style="text-align:center; padding:40px 0px; font-size:20px; color:#992222;"> The Current Version of jQuery:' + version + " <br>Please update your jQuery Version to min. 1.7 in Case you wish to use the Revolution Slider Plugin</div>");
                if (!$.support.transition) $.fn.transition = $.fn.animate;
                $.cssEase["bounce"] = "cubic-bezier(0,1,0.5,1.3)";
                container.find(".caption iframe").each(function () {
                    try {
                        if ($(this).attr("src").indexOf("you") > 0) {
                            var s = document.createElement("script");
                            s.src = "http://www.youtube.com/player_api";
                            var before = document.getElementsByTagName("script")[0];
                            before.parentNode.insertBefore(s, before)
                        }
                    } catch (e) {}
                });
                container.find(".caption iframe").each(function () {
                    try {
                        if ($(this).attr("src").indexOf("vim") > 0) {
                            var f = document.createElement("script");
                            f.src = "http://a.vimeocdn.com/js/froogaloop2.min.js";
                            var before = document.getElementsByTagName("script")[0];
                            before.parentNode.insertBefore(f, before)
                        }
                    } catch (e) {}
                });
                if (opt.shuffle == "on") for (var u = 0; u < container.find(">ul:first-child >li").length; u++) {
                    var it = Math.round(Math.random() * container.find(">ul:first-child >li").length);
                    container.find(">ul:first-child >li:eq(" + it + ")").prependTo(container.find(">ul:first-child"))
                }
                opt.slots = 4;
                opt.act = -1;
                opt.next = 0;
                opt.origcd = opt.delay;
                opt.firefox13 = $.browser.mozilla && (parseInt($.browser.version, 0) == 13 || parseInt($.browser.version, 0) == 14 || parseInt($.browser.version, 0) == 15 || parseInt($.browser.version, 0) == 16);
                opt.ie = $.browser.msie && parseInt($.browser.version, 0) < 9;
                opt.ie9 = $.browser.msie && parseInt($.browser.version, 0) == 9;
                if (opt.navOffsetHorizontal == undefined) opt.navOffsetHorizontal = 0;
                if (opt.navOffsetVertical == undefined) opt.navOffsetVertical = 0;
                opt.navOH = opt.navOffsetHorizontal;
                opt.navOV = opt.navOffsetVertical;
                container.append('<div class="tp-loader"></div>');
                var bt = container.find(".tp-bannertimer");
                if (bt.length > 0) bt.css({
                    "width": "0%"
                });
                container.addClass("tp-simpleresponsive");
                opt.container = container;
                opt.slideamount = container.find(">ul:first >li").length;
                if (container.height() == 0) container.height(opt.startheight);
                if (opt.startwidth == undefined || opt.startwidth == 0) opt.startwidth = container.width();
                if (opt.startheight == undefined || opt.startheight == 0) opt.startheight = container.height();
                opt.width = container.width();
                
                opt.height = container.height();
                opt.bw = opt.startwidth / container.width();
                opt.bh = opt.startheight / container.height();
                if (opt.width != opt.startwidth) {
                    opt.height = Math.round(opt.startheight * (opt.width / opt.startwidth));
                    container.height(opt.height)
                }
                if (opt.shadow != 0) {
                    container.parent().append('<div class="tp-bannershadow tp-shadow' + opt.shadow + '"></div>');
                    container.parent().find(".tp-bannershadow").css({
                        "width": opt.width
                    })
                }
                container.waitForImages(function () {
                    prepareSlides(container, opt);
                    if (opt.slideamount > 1) createBullets(container, opt);
                    if (opt.slideamount > 1) createThumbs(container, opt);
                    if (opt.slideamount > 1) createArrows(container, opt);
                    swipeAction(container, opt);
                    if (opt.hideThumbs > 0) hideThumbs(container, opt);
                    container.waitForImages(function () {
                        container.find(".tp-loader").fadeOut(400);
                        setTimeout(function () {
                            swapSlide(container, opt);
                            if (opt.slideamount > 1) countDown(container, opt)
                        }, 1E3)
                    })
                });
                $(window).resize(function () {
                    if (container.outerWidth(true) != opt.width) containerResized(container, opt)
                })
            })
        },
        revpause: function (options) {
            return this.each(function () {
                var container = $(this);
                container.data("conthover", 1);
                container.data("conthover-changed", 1);
                container.trigger("revolution.slide.onpause");
                var bt = container.parent().find(".tp-bannertimer");
                bt.stop()
            })
        },
        revresume: function (options) {
            return this.each(function () {
                var container = $(this);
                container.data("conthover", 0);
                container.data("conthover-changed", 1);
                container.trigger("revolution.slide.onresume");
                var bt = container.parent().find(".tp-bannertimer");
                var opt = bt.data("opt");
                bt.animate({
                    "width": "100%"
                }, {
                    duration: opt.delay - opt.cd - 100,
                    queue: false,
                    easing: "linear"
                })
            })
        },
        revnext: function (options) {
            return this.each(function () {
                var container = $(this);
                container.parent().find(".tp-rightarrow").click()
            })
        },
        revprev: function (options) {
            return this.each(function () {
                var container = $(this);
                container.parent().find(".tp-leftarrow").click()
            })
        },
        revmaxslide: function (options) {
            return $(this).find(">ul:first-child >li").length
        },
        revshowslide: function (slide) {
            return this.each(function () {
                var container = $(this);
                container.data("showus", slide);
                container.parent().find(".tp-rightarrow").click()
            })
        }
    });

    function containerResized(container, opt) {
        container.find(".defaultimg").each(function (i) {
            setSize($(this), opt);
            opt.height = Math.round(opt.startheight * (opt.width / opt.startwidth));
         
            container.height(opt.height);
            setSize($(this), opt);
            try {
                container.parent().find(".tp-bannershadow").css({
                    "width": opt.width
                })
            } catch (e) {}
            var actsh = container.find(">ul >li:eq(" + opt.act + ") .slotholder");
            var nextsh = container.find(">ul >li:eq(" + opt.next + ") .slotholder");
            removeSlots(container, opt);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".defaultimg").css({
                "opacity": 1
            });
            setCaptionPositions(container, opt);
            var nextli = container.find(">ul >li:eq(" + opt.next + ")");
            container.find(".caption").each(function () {
                $(this).stop(true, true)
            });
            animateTheCaptions(nextli, opt);
            restartBannerTimer(opt, container)
        })
    }
    function restartBannerTimer(opt, container) {
        opt.cd = 0;
        if (opt.videoplaying != true) {
            var bt = container.find(".tp-bannertimer");
            if (bt.length > 0) {
                bt.stop();
                bt.css({
                    "width": "0%"
                });
                bt.animate({
                    "width": "100%"
                }, {
                    duration: opt.delay - 100,
                    queue: false,
                    easing: "linear"
                })
            }
            clearTimeout(opt.thumbtimer);
            opt.thumbtimer = setTimeout(function () {
                moveSelectedThumb(container);
                setBulPos(container, opt)
            }, 200)
        }
    }
    function callingNewSlide(opt, container) {
        opt.cd = 0;
        swapSlide(container, opt);
        var bt = container.find(".tp-bannertimer");
        if (bt.length > 0) {
            bt.stop();
            bt.css({
                "width": "0%"
            });
            bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - 100,
                queue: false,
                easing: "linear"
            })
        }
    }
    function createThumbs(container, opt) {
        var cap = container.parent();
        if (opt.navigationType == "thumb" || opt.navsecond == "both" || opt.navigationType == "bullet") cap.append('<div class="tp-cornerleft"></div><div class="tp-bullets tp-thumbs ' + opt.navigationStyle + '"><div class="tp-mask"><div class="tp-thumbcontainer"></div></div></div><div class="tp-cornerright"></div>');
        var bullets = cap.find(".tp-bullets.tp-thumbs .tp-mask .tp-thumbcontainer");
        var bup = bullets.parent();
        bup.width(opt.thumbWidth * opt.thumbAmount);
        bup.height(opt.thumbHeight);         
        bup.parent().width(opt.thumbWidth * opt.thumbAmount);
        bup.parent().height(opt.thumbHeight);
        container.find(">ul:first >li").each(function (i) {
            var li = container.find(">ul:first >li:eq(" + i + ")");
            if (li.data("thumb") != undefined) var src = li.data("thumb");
            else var src = li.find("img:first").attr("src");
            bullets.append('<div class="bullet thumb"><img src="' + src + '"></div>');
            var bullet = bullets.find(".bullet:first")
        });
        bullets.append('<div style="clear:both"></div>');
        var minwidth = 1E3;
        bullets.find(".bullet").each(function (i) {
            var bul = $(this);
            if (i == opt.slideamount - 1) bul.addClass("last");
            if (i == 0) bul.addClass("first");
            bul.width(opt.thumbWidth);
            bul.height(opt.thumbHeight);
            if (minwidth > bul.outerWidth(true)) minwidth = bul.outerWidth(true);
            bul.click(function () {
                if (opt.transition == 0 && bul.index() != opt.act) {
                    opt.next = bul.index();
                    callingNewSlide(opt, container)
                }
            })
        });
        var max = minwidth * container.find(">ul:first >li").length;
        var thumbconwidth = bullets.parent().width();
        opt.thumbWidth = minwidth;
        if (thumbconwidth < max) {
            $(document).mousemove(function (e) {
                $("body").data("mousex", e.pageX)
            });
            bullets.parent().mouseenter(function () {
                var $this = $(this);
                $this.addClass("over");
                var offset = $this.offset();
                var x = $("body").data("mousex") - offset.left;
                var thumbconwidth = $this.width();
                var minwidth = $this.find(".bullet:first").outerWidth(true);
                var max = minwidth * container.find(">ul:first >li").length;
                var diff = max - thumbconwidth + 15;
                var steps = diff / thumbconwidth;
                x = x - 30;
                var pos = 0 - x * steps;
                if (pos > 0) pos = 0;
                if (pos < 0 - max + thumbconwidth) pos = 0 - max + thumbconwidth;
                moveThumbSliderToPosition($this, pos, 200)
            });
            bullets.parent().mousemove(function () {
                var $this = $(this);
                var offset = $this.offset();
                var x = $("body").data("mousex") - offset.left;
                var thumbconwidth = $this.width();
                var minwidth = $this.find(".bullet:first").outerWidth(true);
                var max = minwidth * container.find(">ul:first >li").length;
                var diff = max - thumbconwidth + 15;
                var steps = diff / thumbconwidth;
                x = x - 30;
                var pos = 0 - x * steps;
                if (pos > 0) pos = 0;
                if (pos < 0 - max + thumbconwidth) pos = 0 - max + thumbconwidth;
                moveThumbSliderToPosition($this, pos, 0)
            });
            bullets.parent().mouseleave(function () {
                var $this = $(this);
                $this.removeClass("over");
                moveSelectedThumb(container)
            })
        }
    }
    function moveSelectedThumb(container) {
        var bullets = container.parent().find(".tp-bullets.tp-thumbs .tp-mask .tp-thumbcontainer");
        var $this = bullets.parent();
        var offset = $this.offset();
        var minwidth = $this.find(".bullet:first").outerWidth(true);
        var x = $this.find(".bullet.selected").index() * minwidth;
        var thumbconwidth = $this.width();
        var minwidth = $this.find(".bullet:first").outerWidth(true);
        var max = minwidth * container.find(">ul:first >li").length;
        var diff = max - thumbconwidth;
        var steps = diff / thumbconwidth;
        var pos = 0 - x;
        if (pos > 0) pos = 0;
        if (pos < 0 - max + thumbconwidth) pos = 0 - max + thumbconwidth;
        if (!$this.hasClass("over")) moveThumbSliderToPosition($this, pos, 200)
    }
    function moveThumbSliderToPosition($this, pos, speed) {
        $this.stop();
        $this.find(".tp-thumbcontainer").animate({
            "left": pos + "px"
        }, {
            duration: speed,
            queue: false
        })
    }
    function createBullets(container, opt) {
        if (opt.navigationType == "bullet" || opt.navigationType == "both") container.parent().append('<div class="tp-bullets simplebullets ' + opt.navigationStyle + '"></div>');
        var bullets = container.parent().find(".tp-bullets");
        container.find(">ul:first >li").each(function (i) {
            var src = container.find(">ul:first >li:eq(" + i + ") img:first").attr("src");
            bullets.append('<div class="bullet"></div>');
            var bullet = bullets.find(".bullet:first")
        });
        bullets.find(".bullet").each(function (i) {
            var bul = $(this);
            if (i == opt.slideamount - 1) bul.addClass("last");
            if (i == 0) bul.addClass("first");
            bul.click(function () {
                if (opt.transition == 0 && bul.index() != opt.act) {
                    opt.next = bul.index();
                    callingNewSlide(opt, container)
                }
            })
        });
        bullets.append('<div style="clear:both"></div>');
        setBulPos(container, opt);
        $("#unvisible_button").click(function () {
            opt.navigationArrows = $(".select_navarrows .selected").data("value");
            opt.navigationType = $(".select_navigationtype .selected").data("value");
            opt.hideThumbs = $(".select_navshow .selected").data("value");
            container.data("hidethumbs", opt.hideThumbs);
            var bhd = $(".select_bhposition .dragger");
            opt.navOffsetHorizontal = Math.round((bhd.data("max") - bhd.data("min")) * (bhd.position().left / 410) + bhd.data("min"));
            var bvd = $(".select_bvposition .dragger");
            opt.navOffsetVertical = Math.round((bvd.data("max") - bvd.data("min")) * (bvd.position().left / 410) + bvd.data("min"));
            var btr = $(".select_slidetime .dragger");
            opt.delay2 = Math.round(((btr.data("max") - btr.data("min")) * (btr.position().left / 410) + btr.data("min")) * 1E3);
            if (opt.delay2 != opt.delay) {
                opt.delay = opt.delay2;
                opt.origcd = opt.delay;
                opt.cd = 0;
                var bt = container.find(".tp-bannertimer");
                if (bt.length > 0) {
                    bt.stop();
                    bt.css({
                        "width": "0%"
                    });
                    bt.animate({
                        "width": "100%"
                    }, {
                        duration: opt.delay - 100,
                        queue: false,
                        easing: "linear"
                    })
                }
            }
            opt.onHoverStop = $(".select_hovers .selected").data("value");
            setBulPos(container, opt);
            setTimeout(function () {
                setBulPos(container, opt)
            }, 100)
        })
    }
    function createArrows(container, opt) {
        var bullets = container.find(".tp-bullets");
        var hidden = "";
        if (opt.navigationArrow == "none") hidden = "visibility:none";
        container.parent().append('<div style="' + hidden + '" class="tp-leftarrow tparrows ' + opt.navigationStyle + '"></div>');
        container.parent().append('<div style="' + hidden + '" class="tp-rightarrow tparrows ' + opt.navigationStyle + '"></div>');
        container.parent().find(".tp-rightarrow").click(function () {
            if (opt.transition == 0) {
                if (container.data("showus") != undefined && container.data("showus") != -1) opt.next = container.data("showus") - 1;
                else opt.next = opt.next + 1;
                container.data("showus", -1);
                if (opt.next >= opt.slideamount) opt.next = 0;
                if (opt.next < 0) opt.next = 0;
                if (opt.act != opt.next) callingNewSlide(opt, container)
            }
        });
        container.parent().find(".tp-leftarrow").click(function () {
            if (opt.transition == 0) {
                opt.next = opt.next - 1;
                opt.leftarrowpressed = 1;
                if (opt.next < 0) opt.next = opt.slideamount - 1;
                callingNewSlide(opt, container)
            }
        });
        setBulPos(container, opt)
    }
    function swipeAction(container, opt) {
        if (opt.touchenabled == "on") container.swipe({
            data: container,
            swipeRight: function () {
                if (opt.transition == 0) {
                    opt.next = opt.next - 1;
                    opt.leftarrowpressed = 1;
                    if (opt.next < 0) opt.next = opt.slideamount - 1;
                    callingNewSlide(opt, container)
                }
            },
            swipeLeft: function () {
                if (opt.transition == 0) {
                    opt.next = opt.next + 1;
                    if (opt.next == opt.slideamount) opt.next = 0;
                    callingNewSlide(opt, container)
                }
            },
            allowPageScroll: "auto"
        })
    }
    function hideThumbs(container, opt) {
        var bullets = container.parent().find(".tp-bullets");
        var ca = container.parent().find(".tparrows");
        if (bullets == null) {
            container.append('<div class=".tp-bullets"></div>');
            var bullets = container.parent().find(".tp-bullets")
        }
        if (ca == null) {
            container.append('<div class=".tparrows"></div>');
            var ca = container.parent().find(".tparrows")
        }
        container.data("hidethumbs", opt.hideThumbs);
        if (opt.ie) {
            bullets.css({
                "visibility": "hidden"
            });
            ca.css({
                "visibility": "hidden"
            })
        } else {
            try {
                bullets.css({
                    "opacity": 0
                })
            } catch (e) {}
            try {
                ca.css({
                    "opacity": 0
                })
            } catch (e) {}
        }
        bullets.hover(function () {
            bullets.addClass("hovered");
            clearTimeout(container.data("hidethumbs"));
            bullets.animate({
                "opacity": 1
            }, {
                duration: 200,
                queue: false
            });
            ca.animate({
                "opacity": 1
            }, {
                duration: 200,
                queue: false
            })
        }, function () {
            bullets.removeClass("hovered");
            if (!container.hasClass("hovered") && !bullets.hasClass("hovered")) container.data("hidethumbs", setTimeout(function () {
                if (opt.ie) {
                    bullets.css({
                        "visibility": "hidden"
                    });
                    ca.css({
                        "visibility": "hidden"
                    })
                } else {
                    bullets.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    });
                    ca.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    })
                }
            }, opt.hideThumbs))
        });
        ca.hover(function () {
            bullets.addClass("hovered");
            clearTimeout(container.data("hidethumbs"));
            if (opt.ie) {
                bullets.css({
                    "visibility": "visible"
                });
                ca.css({
                    "visibility": "visible"
                })
            } else {
                bullets.animate({
                    "opacity": 1
                }, {
                    duration: 200,
                    queue: false
                });
                ca.animate({
                    "opacity": 1
                }, {
                    duration: 200,
                    queue: false
                })
            }
        }, function () {
            bullets.removeClass("hovered");
            if (!container.hasClass("hovered") && !bullets.hasClass("hovered")) container.data("hidethumbs", setTimeout(function () {
                if (opt.ie) {
                    bullets.css({
                        "visibility": "hidden"
                    });
                    ca.css({
                        "visibility": "hidden"
                    })
                } else {
                    bullets.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    });
                    ca.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    })
                }
            }, opt.hideThumbs))
        });
        container.live("mouseenter", function () {
            container.addClass("hovered");
            clearTimeout(container.data("hidethumbs"));
            if (opt.ie) {
                bullets.css({
                    "visibility": "visible"
                });
                ca.css({
                    "visibility": "visible"
                })
            } else {
                bullets.animate({
                    "opacity": 1
                }, {
                    duration: 200,
                    queue: false
                });
                ca.animate({
                    "opacity": 1
                }, {
                    duration: 200,
                    queue: false
                })
            }
        });
        container.live("mouseleave", function () {
            container.removeClass("hovered");
            if (!container.hasClass("hovered") && !bullets.hasClass("hovered")) container.data("hidethumbs", setTimeout(function () {
                if (opt.ie) {
                    bullets.css({
                        "visibility": "hidden"
                    });
                    ca.css({
                        "visibility": "hidden"
                    })
                } else {
                    bullets.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    });
                    ca.animate({
                        "opacity": 0
                    }, {
                        duration: 200,
                        queue: false
                    })
                }
            }, opt.hideThumbs))
        })
    }
    function setBulPos(container, opt) {
        if (opt.navigationType == "both") {
            opt.navigationType = "bullet";
            opt.navsecond = "both"
        }
        if (opt.navigationType == "none" && opt.navigationArrows != "none") opt.navigationArrows = "verticalcentered";
        opt.navOH = opt.navOffsetHorizontal * opt.bw;
        opt.navOV = opt.navOffsetVertical * opt.bh;
        if (opt.bw != 1) opt.navOH = 0;
        var cap = container.parent();
        var la = cap.find(".tp-leftarrow");
        var ra = cap.find(".tp-rightarrow");
        if (opt.navigationType == "bullet") {
            var bullets = cap.find(".tp-bullets.simplebullets");
            var cornerLeft = cap.find(".tp-cornerleft");
            var cornerRight = cap.find(".tp-cornerright");
            bullets.css({
                "visibility": "visible"
            });
            try {
                cap.find(".tp-thumbs").css({
                    "visibility": "hidden"
                });
                if (opt.ie) cap.find(".tp-thumbs").remove()
            } catch (e) {}
            var fulllong = bullets.width();
            if (!bullets.hasClass("tp-thumbs")) {
                fulllong = 0;
                bullets.find(".bullet").each(function () {
                    fulllong = fulllong + $(this).outerWidth(true)
                });
                bullets.css({
                    "width": fulllong + "px"
                })
            }
            var ldiff = cap.outerWidth() - opt.width;
            bullets.css({
                "left": opt.navOH + ldiff / 2 + (opt.width / 2 - fulllong / 2) - 10 + "px",
                "bottom": opt.navOV + "px"
            });
            cornerLeft.css({
                "left": opt.navOH + ldiff / 2 + (opt.width / 2 - fulllong / 2) - 40 + "px",
                "bottom": opt.navOV + "px"
            });
            cornerRight.css({
                "left": opt.navOH + ldiff / 2 + (opt.width / 2 - fulllong / 2) + fulllong + 10 + "px",
                "bottom": opt.navOV + "px"
            });
            if (opt.navigationArrows == "nexttobullets") {
                la.removeClass("large");
                ra.removeClass("large");
                la.removeClass("thumbswitharrow");
                ra.removeClass("thumbswitharrow");
                la.css({
                    "visibility": "visible"
                });
                ra.css({
                    "visibility": "visible"
                });
                var diff = 0;
                la.css({
                    "position": "absolute",
                    "left": bullets.position().left - la.outerWidth(true) + "px",
                    "top": bullets.position().top + "px"
                });
                ra.css({
                    "position": "absolute",
                    "left": bullets.outerWidth(true) + bullets.position().left + "px",
                    "top": bullets.position().top + "px"
                })
            } else if (opt.navigationArrows == "verticalcentered") {
                la.addClass("large");
                ra.addClass("large");
                la.css({
                    "visibility": "visible"
                });
                ra.css({
                    "visibility": "visible"
                });
                var decorh = cap.outerHeight();
                la.css({
                    "position": "absolute",
                    "left": ldiff / 2 + "px",
                    "top": decorh / 2 + "px"
                });
                ra.css({
                    "position": "absolute",
                    "left": opt.width - ra.outerWidth() + ldiff / 2 + "px",
                    "top": decorh / 2 + "px"
                })
            } else {
                la.css({
                    "visibility": "hidden"
                });
                ra.css({
                    "visibility": "hidden"
                })
            }
        } else if (opt.navigationType == "thumb") {
            var thumbs = cap.find(".tp-thumbs");
            try {
                cap.find(".tp-bullets").css({
                    "visibility": "hidden"
                })
            } catch (e) {}
            thumbs.css({
                "visibility": "visible"
            });
            var decorh = thumbs.parent().outerHeight();
            var ldiff = cap.outerWidth() - opt.width;
            thumbs.css({
                "left": opt.navOH + (opt.width / 2 - thumbs.width() / 2) + "px"
            });
            thumbs.css({
                "bottom": 0 - thumbs.outerHeight(true) + opt.navOV + "px"
            });
            if (opt.navigationArrows == "verticalcentered") {
                la.css({
                    "visibility": "visible"
                });
                ra.css({
                    "visibility": "visible"
                });
                la.addClass("large");
                ra.addClass("large");
                la.css({
                    "position": "absolute",
                    "left": ldiff / 2 + "px",
                    "top": cap.outerHeight() / 2 + "px"
                });
                ra.css({
                    "position": "absolute",
                    "left": opt.width - ra.outerWidth() + ldiff / 2 + "px",
                    "top": cap.outerHeight() / 2 + "px"
                })
            } else {
                la.css({
                    "visibility": "hidden"
                });
                ra.css({
                    "visibility": "hidden"
                })
            }
        } else if (opt.navigationType == "none") {
            try {
                cap.find(".tp-bullets").css({
                    "visibility": "hidden"
                })
            } catch (e) {}
            try {
                cap.find(".tp-thumbs").css({
                    "visibility": "hidden"
                })
            } catch (e) {}
            if (opt.navigationArrows != "none") {
                var ldiff = cap.outerWidth() - opt.width;
                la.css({
                    "visibility": "visible"
                });
                ra.css({
                    "visibility": "visible"
                });
                la.addClass("large");
                ra.addClass("large");
                la.css({
                    "position": "absolute",
                    "left": ldiff / 2 + "px",
                    "top": cap.outerHeight() / 2 + "px"
                });
                ra.css({
                    "position": "absolute",
                    "left": opt.width - ra.outerWidth() + ldiff / 2 + "px",
                    "top": cap.outerHeight() / 2 + "px"
                })
            } else {
                la.css({
                    "visibility": "hidden"
                });
                ra.css({
                    "visibility": "hidden"
                })
            }
        }
    }
    function setSize(img, opt) {
        opt.width = parseInt(opt.container.width(), 0);
        opt.height = parseInt(opt.container.height(), 0);
        opt.bw = opt.width / opt.startwidth;
        opt.bh = opt.height / opt.startheight;         
        if (opt.bh > 1) {
            opt.bw = 1;
            opt.bh = 1
        }
        if (img.data("orgw") != undefined) {
            img.width(img.data("orgw"));
            img.height(img.data("orgh"))
        }
        var fw = opt.width / img.width();
        var fh = opt.height / img.height();
        opt.fw = fw;
        opt.fh = fh;
        if (img.data("orgw") == undefined) {
            img.data("orgw", img.width());
            img.data("orgh", img.height())
        }
        if (opt.fullWidth == "on") {
            var cow = opt.container.parent().width();
            var coh = opt.container.parent().height();
            var ffh = coh / img.data("orgh");
            var ffw = cow / img.data("orgw");
            img.width(img.width() * ffh);
            img.height(coh);
            if (img.width() < cow) {
                img.width(cow + 50);
                var ffw = img.width() / img.data("orgw");
                img.height(img.data("orgh") * ffw)
            }
            if (img.width() > cow) {
                img.data("fxof", cow / 2 - img.width() / 2);
                img.css({
                    "position": "absolute",
                    "left": img.data("fxof") + "px"
                })
            }
        } else {
            img.width(opt.width);
            img.height(img.height() * fw);
            if (img.height() < opt.height && img.height() != 0 && img.height() != null) {
                img.height(opt.height);
                img.width(img.data("orgw") * fh)
            }
        }
        img.data("neww", img.width());
        img.data("newh", img.height());
        if (opt.fullWidth == "on") opt.slotw = Math.ceil(img.width() / opt.slots);
        else opt.slotw = Math.ceil(opt.width / opt.slots);
        opt.sloth = Math.ceil(opt.height / opt.slots)
    }
    function prepareSlides(container, opt) {
        container.find(".caption").each(function () {
            $(this).addClass($(this).data("transition"));
            $(this).addClass("start")
        });
        container.find(">ul:first >li").each(function (j) {
            var li = $(this);
            if (li.data("link") != undefined) {
                var link = li.data("link");
                var target = "_self";
                if (li.data("target") != undefined) target = li.data("target");
                li.append('<div class="caption sft slidelink" data-x="0" data-y="0" data-start="0"><a target="' + target + '" href="' + link + '"><div></div></a></div>')
            }
        });
        container.find(">ul:first >li >img").each(function (j) {
            var img = $(this);
            img.addClass("defaultimg");
            setSize(img, opt);
            setSize(img, opt);
            img.wrap('<div class="slotholder"></div>');
            img.css({
                "opacity": 0
            });
            img.data("li-id", j)
        })
    }
    function prepareOneSlide(slotholder, opt, visible) {
        var sh = slotholder;
        var img = sh.find("img");
        setSize(img, opt);
        var src = img.attr("src");
        var w = img.data("neww");
        var h = img.data("newh");
        var fulloff = img.data("fxof");
        if (fulloff == undefined) fulloff = 0;
        var off = 0;
        if (!visible) var off = 0 - opt.slotw;
        for (var i = 0; i < opt.slots; i++) sh.append('<div class="slot" style="position:absolute;top:0px;left:' + (fulloff + i * opt.slotw) + "px;overflow:hidden;width:" + opt.slotw + "px;height:" + h + 'px"><div class="slotslide" style="position:absolute;top:0px;left:' + off + "px;width:" + opt.slotw + "px;height:" + h + 'px;overflow:hidden;"><img style="position:absolute;top:0px;left:' + (0 - i * opt.slotw) + "px;width:" + w + "px;height:" + h + 'px" src="' + src + '"></div></div>')
    }
    function prepareOneSlideV(slotholder, opt, visible) {
        var sh = slotholder;
        var img = sh.find("img");
        setSize(img, opt);
        var src = img.attr("src");
        var w = img.data("neww");
        var h = img.data("newh");
        var fulloff = img.data("fxof");
        if (fulloff == undefined) fulloff = 0;
        var off = 0;
        if (!visible) var off = 0 - opt.sloth;
        for (var i = 0; i < opt.slots; i++) sh.append('<div class="slot" style="position:absolute;top:' + i * opt.sloth + "px;left:" + fulloff + "px;overflow:hidden;width:" + w + "px;height:" + opt.sloth + 'px"><div class="slotslide" style="position:absolute;top:' + off + "px;left:0px;width:" + w + "px;height:" + opt.sloth + 'px;overflow:hidden;"><img style="position:absolute;top:' + (0 - i * opt.sloth) + "px;left:0px;width:" + w + "px;height:" + h + 'px" src="' + src + '"></div></div>')
    }
    function prepareOneSlideBox(slotholder, opt, visible) {
        var sh = slotholder;
        var img = sh.find("img");
        setSize(img, opt);
        var src = img.attr("src");
        var w = img.data("neww");
        var h = img.data("newh");
        var fulloff = img.data("fxof");
        if (fulloff == undefined) fulloff = 0;
        var off = 0;
        var basicsize = 0;
        if (opt.sloth > opt.slotw) basicsize = opt.sloth;
        else basicsize = opt.slotw;
        if (!visible) var off = 0 - basicsize;
        opt.slotw = basicsize;
        opt.sloth = basicsize;
        var x = 0;
        var y = 0;
        for (var j = 0; j < opt.slots; j++) {
            y = 0;
            for (var i = 0; i < opt.slots; i++) {
                sh.append('<div class="slot" ' + 'style="position:absolute;' + "top:" + y + "px;" + "left:" + (fulloff + x) + "px;" + "width:" + basicsize + "px;" + "height:" + basicsize + "px;" + 'overflow:hidden;">' + '<div class="slotslide" data-x="' + x + '" data-y="' + y + '" ' + 'style="position:absolute;' + "top:" + 0 + "px;" + "left:" + 0 + "px;" + "width:" + basicsize + "px;" + "height:" + basicsize + "px;" + 'overflow:hidden;">' + '<img style="position:absolute;' + "top:" + (0 - y) + "px;" + "left:" + (0 - x) + "px;" + "width:" + w + "px;" + "height:" + h + 'px"' + 'src="' + src + '"></div></div>');
                y = y + basicsize
            }
            x = x + basicsize
        }
    }
    function removeSlots(container, opt, time) {
        if (time == undefined) time == 80;
        setTimeout(function () {
            container.find(".slotholder .slot").each(function () {
                clearTimeout($(this).data("tout"));
                $(this).remove()
            });
            opt.transition = 0
        }, time)
    }
    function setCaptionPositions(container, opt) {
        var actli = container.find(">li:eq(" + opt.act + ")");
        var nextli = container.find(">li:eq(" + opt.next + ")");
        var nextcaption = nextli.find(".caption");
        if (nextcaption.find("iframe") == 0) if (nextcaption.hasClass("hcenter")) nextcaption.css({
            "height": opt.height + "px",
            "top": "0px",
            "left": opt.width / 2 - nextcaption.outerWidth() / 2 + "px"
        });
        else if (nextcaption.hasClass("vcenter")) nextcaption.css({
            "width": opt.width + "px",
            "left": "0px",
            "top": opt.height / 2 - nextcaption.outerHeight() / 2 + "px"
        })
    }
    function swapSlide(container, opt) {
        opt.transition = 1;
        opt.videoplaying = false;
        try {
            var actli = container.find(">ul:first-child >li:eq(" + opt.act + ")")
        } catch (e) {
            var actli = container.find(">ul:first-child >li:eq(1)")
        }
        var nextli = container.find(">ul:first-child >li:eq(" + opt.next + ")");
        var actsh = actli.find(".slotholder");
        var nextsh = nextli.find(".slotholder");
        actli.css({
            "visibility": "visible"
        });
        nextli.css({
            "visibility": "visible"
        });
        if ($.browser.msie && $.browser.version < 9) {
            if (nextli.data("transition") == "boxfade") nextli.data("transition", "boxslide");
            if (nextli.data("transition") == "slotfade-vertical") nextli.data("transition", "slotzoom-vertical");
            if (nextli.data("transition") == "slotfade-horizontal") nextli.data("transition", "slotzoom-horizontal")
        }
        if (nextli.data("delay") != undefined) {
            opt.cd = 0;
            opt.delay = nextli.data("delay")
        } else opt.delay = opt.origcd;
        actli.css({
            "left": "0px",
            "top": "0px"
        });
        nextli.css({
            "left": "0px",
            "top": "0px"
        });
        var nexttrans = 0;
        if (nextli.data("transition") == "boxslide") nexttrans = 0;
        else if (nextli.data("transition") == "boxfade") nexttrans = 1;
        else if (nextli.data("transition") == "slotslide-horizontal") nexttrans = 2;
        else if (nextli.data("transition") == "slotslide-vertical") nexttrans = 3;
        else if (nextli.data("transition") == "curtain-1") nexttrans = 4;
        else if (nextli.data("transition") == "curtain-2") nexttrans = 5;
        else if (nextli.data("transition") == "curtain-3") nexttrans = 6;
        else if (nextli.data("transition") == "slotzoom-horizontal") nexttrans = 7;
        else if (nextli.data("transition") == "slotzoom-vertical") nexttrans = 8;
        else if (nextli.data("transition") == "slotfade-horizontal") nexttrans = 9;
        else if (nextli.data("transition") == "slotfade-vertical") nexttrans = 10;
        else if (nextli.data("transition") == "fade") nexttrans = 11;
        else if (nextli.data("transition") == "slideleft") nexttrans = 12;
        else if (nextli.data("transition") == "slideup") nexttrans = 13;
        else if (nextli.data("transition") == "slidedown") nexttrans = 14;
        else if (nextli.data("transition") == "slideright") nexttrans = 15;
        else if (nextli.data("transition") == "papercut") nexttrans = 16;
        else if (nextli.data("transition") == "3dcurtain-horizontal") nexttrans = 17;
        else if (nextli.data("transition") == "3dcurtain-vertical") nexttrans = 18;
        else if (nextli.data("transition") == "cubic") nexttrans = 19;
        else if (nextli.data("transition") == "flyin") nexttrans = 20;
        else if (nextli.data("transition") == "turnoff") nexttrans = 21;
        else {
            nexttrans = Math.round(Math.random() * 20);
            nextli.data("slotamount", Math.round(Math.random() * 12 + 4))
        }
        var direction = -1;
        if (opt.leftarrowpressed == 1 || opt.act > opt.next) direction = 1;
        if (nextli.data("transition") == "slidehorizontal") {
            nexttrans = 12;
            if (opt.leftarrowpressed == 1) nexttrans = 15
        }
        if (nextli.data("transition") == "slidevertical") {
            nexttrans = 13;
            if (opt.leftarrowpressed == 1) nexttrans = 14
        }
        opt.leftarrowpressed = 0;
        if (nexttrans > 21) nexttrans = 21;
        if (nexttrans < 0) nexttrans = 0;
        if (!$.support.transition && nexttrans > 16) {
            nexttrans = Math.round(Math.random() * 16);
            nextli.data("slotamount", Math.round(Math.random() * 12 + 4))
        }
        if (opt.ie && (nexttrans == 17 || nexttrans == 16 || nexttrans == 2 || nexttrans == 3 || nexttrans == 9 || nexttrans == 10 || nexttrans == 11)) nexttrans = Math.round(Math.random() * 3 + 12);
        var masterspeed = 300;
        if (nextli.data("masterspeed") != undefined && nextli.data("masterspeed") > 99 && nextli.data("masterspeed") < 4001) masterspeed = nextli.data("masterspeed");
        container.parent().find(".bullet").each(function () {
            var bul = $(this);
            bul.removeClass("selected");
            if (bul.index() == opt.next) bul.addClass("selected")
        });
        container.find(">li").each(function () {
            var li = $(this);
            if (li.index != opt.act && li.index != opt.next) li.css({
                "z-index": 16
            })
        });
        actli.css({
            "z-index": 18
        });
        nextli.css({
            "z-index": 20
        });
        nextli.css({
            "opacity": 0
        });
        removeTheCaptions(actli, opt);
        animateTheCaptions(nextli, opt);
        if (nextli.data("slotamount") == undefined || nextli.data("slotamount") < 1) {
            opt.slots = Math.round(Math.random() * 12 + 4);
            if (nextli.data("transition") == "boxslide") opt.slots = Math.round(Math.random() * 6 + 3)
        } else opt.slots = nextli.data("slotamount");
        if (nextli.data("rotate") == undefined) opt.rotate = 0;
        else if (nextli.data("rotate") == 999) opt.rotate = Math.round(Math.random() * 360);
        else opt.rotate = nextli.data("rotate");
        if (!$.support.transition || opt.ie || opt.ie9) opt.rotate = 0;
        if (nexttrans == 0) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideBox(actsh, opt, true);
            prepareOneSlideBox(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                if (opt.ie9) ss.transition({
                    top: 0 - opt.sloth,
                    left: 0 - opt.slotw
                }, 0);
                else ss.transition({
                    top: 0 - opt.sloth,
                    left: 0 - opt.slotw,
                    rotate: opt.rotate
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        top: 0,
                        left: 0,
                        scale: 1,
                        rotate: 0
                    }, masterspeed * 1.5, function () {
                        if (j == opt.slots * opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 15)
            })
        }
        if (nexttrans == 1) {
            if (opt.slots > 5) opt.slots = 5;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideBox(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.css({
                    "opacity": 0
                });
                ss.find("img").css({
                    "opacity": 0
                });
                if (opt.ie9) ss.find("img").transition({
                    "top": Math.random() * opt.slotw - opt.slotw + "px",
                    "left": Math.random() * opt.slotw - opt.slotw + "px"
                }, 0);
                else ss.find("img").transition({
                    "top": Math.random() * opt.slotw - opt.slotw + "px",
                    "left": Math.random() * opt.slotw - opt.slotw + "px",
                    rotate: opt.rotate
                }, 0);
                var rand = Math.random() * 1E3 + (masterspeed + 200);
                if (j == opt.slots * opt.slots - 1) rand = 1500;
                ss.find("img").transition({
                    "opacity": 1,
                    "top": 0 - ss.data("y") + "px",
                    "left": 0 - ss.data("x") + "px",
                    rotate: 0
                }, rand);
                ss.transition({
                    "opacity": 1
                }, rand, function () {
                    if (j == opt.slots * opt.slots - 1) {
                        removeSlots(container, opt);
                        nextsh.find(".defaultimg").css({
                            "opacity": 1
                        });
                        if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                            "opacity": 0
                        });
                        opt.act = opt.next;
                        moveSelectedThumb(container)
                    }
                })
            })
        }
        if (nexttrans == 2) {
            masterspeed = masterspeed + 200;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function () {
                var ss = $(this);
                ss.transit({
                    "left": opt.slotw + "px",
                    rotate: 0 - opt.rotate
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            });
            nextsh.find(".slotslide").each(function () {
                var ss = $(this);
                if (opt.ie9) ss.transit({
                    "left": 0 - opt.slotw + "px"
                }, 0);
                else ss.transit({
                    "left": 0 - opt.slotw + "px",
                    rotate: opt.rotate
                }, 0);
                ss.transit({
                    "left": "0px",
                    rotate: 0
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    if ($.browser.msie && $.browser.version < 9) actsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            })
        }
        if (nexttrans == 3) {
            masterspeed = masterspeed + 200;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideV(actsh, opt, true);
            prepareOneSlideV(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function () {
                var ss = $(this);
                ss.transit({
                    "top": opt.sloth + "px",
                    rotate: opt.rotate
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            });
            nextsh.find(".slotslide").each(function () {
                var ss = $(this);
                if (opt.ie9) ss.transit({
                    "top": 0 - opt.sloth + "px"
                }, 0);
                else ss.transit({
                    "top": 0 - opt.sloth + "px",
                    rotate: opt.rotate
                }, 0);
                ss.transit({
                    "top": "0px",
                    rotate: 0
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            })
        }
        if (nexttrans == 4) {
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                ss.transit({
                    "top": 0 + opt.height + "px",
                    "opacity": 1,
                    rotate: opt.rotate
                }, masterspeed + i * (70 - opt.slots))
            });
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                if (opt.ie9) ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0
                }, 0);
                else ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0,
                    rotate: opt.rotate
                }, 0);
                ss.transition({
                    "top": "0px",
                    "opacity": 1,
                    rotate: 0
                }, masterspeed + i * (70 - opt.slots), function () {
                    if (i == opt.slots - 1) {
                        removeSlots(container, opt);
                        nextsh.find(".defaultimg").css({
                            "opacity": 1
                        });
                        if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                            "opacity": 0
                        });
                        opt.act = opt.next;
                        moveSelectedThumb(container)
                    }
                })
            })
        }
        if (nexttrans == 5) {
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                ss.transition({
                    "top": 0 + opt.height + "px",
                    "opacity": 1,
                    rotate: opt.rotate
                }, masterspeed + (opt.slots - i) * (70 - opt.slots))
            });
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                if (opt.ie9) ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0
                }, 0);
                else ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0,
                    rotate: opt.rotate
                }, 0);
                ss.transition({
                    "top": "0px",
                    "opacity": 1,
                    rotate: 0
                }, masterspeed + (opt.slots - i) * (70 - opt.slots), function () {
                    if (i == 0) {
                        removeSlots(container, opt);
                        nextsh.find(".defaultimg").css({
                            "opacity": 1
                        });
                        if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                            "opacity": 0
                        });
                        opt.act = opt.next;
                        moveSelectedThumb(container)
                    }
                })
            })
        }
        if (nexttrans == 6) {
            nextli.css({
                "opacity": 1
            });
            if (opt.slots < 2) opt.slots = 2;
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                if (i < opt.slots / 2) var tempo = (i + 2) * 60;
                else var tempo = (2 + opt.slots - i) * 60;
                ss.transition({
                    "top": 0 + opt.height + "px",
                    "opacity": 1
                }, masterspeed + tempo)
            });
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                if (opt.ie9) ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0
                }, 0);
                else ss.transition({
                    "top": 0 - opt.height + "px",
                    "opacity": 0,
                    rotate: opt.rotate
                }, 0);
                if (i < opt.slots / 2) var tempo = (i + 2) * 60;
                else var tempo = (2 + opt.slots - i) * 60;
                ss.transition({
                    "top": "0px",
                    "opacity": 1,
                    rotate: 0
                }, masterspeed + tempo, function () {
                    if (i == Math.round(opt.slots / 2)) {
                        removeSlots(container, opt);
                        nextsh.find(".defaultimg").css({
                            "opacity": 1
                        });
                        if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                            "opacity": 0
                        });
                        opt.act = opt.next;
                        moveSelectedThumb(container)
                    }
                })
            })
        }
        if (nexttrans == 7) {
            masterspeed = masterspeed * 3;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function () {
                var ss = $(this).find("img");
                ss.transition({
                    "left": 0 - opt.slotw / 2 + "px",
                    "top": 0 - opt.height / 2 + "px",
                    "width": opt.slotw * 2 + "px",
                    "height": opt.height * 2 + "px",
                    opacity: 0,
                    rotate: opt.rotate
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            });
            /\t\t\t\t\t\t/;
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this).find("img");
                if (opt.ie9) ss.transition({
                    "left": 0 + "px",
                    "top": 0 + "px",
                    opacity: 0
                }, 0);
                else ss.transition({
                    "left": 0 + "px",
                    "top": 0 + "px",
                    opacity: 0,
                    rotate: opt.rotate
                }, 0);
                ss.transition({
                    "left": 0 - i * opt.slotw + "px",
                    "top": 0 + "px",
                    "width": nextsh.find(".defaultimg").data("neww") + "px",
                    "height": nextsh.find(".defaultimg").data("newh") + "px",
                    opacity: 1,
                    rotate: 0
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            })
        }
        if (nexttrans == 8) {
            masterspeed = masterspeed * 3;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideV(actsh, opt, true);
            prepareOneSlideV(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            actsh.find(".slotslide").each(function () {
                var ss = $(this).find("img");
                ss.transition({
                    "left": 0 - opt.width / 2 + "px",
                    "top": 0 - opt.sloth / 2 + "px",
                    "width": opt.width * 2 + "px",
                    "height": opt.sloth * 2 + "px",
                    opacity: 0,
                    rotate: opt.rotate
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            });
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this).find("img");
                if (opt.ie9) ss.transition({
                    "left": 0 + "px",
                    "top": 0 + "px",
                    opacity: 0
                }, 0);
                else ss.transition({
                    "left": 0 + "px",
                    "top": 0 + "px",
                    opacity: 0,
                    rotate: opt.rotate
                }, 0);
                ss.transition({
                    "left": 0 + "px",
                    "top": 0 - i * opt.sloth + "px",
                    "width": nextsh.find(".defaultimg").data("neww") + "px",
                    "height": nextsh.find(".defaultimg").data("newh") + "px",
                    opacity: 1,
                    rotate: 0
                }, masterspeed, function () {
                    removeSlots(container, opt);
                    nextsh.find(".defaultimg").css({
                        "opacity": 1
                    });
                    if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                        "opacity": 0
                    });
                    opt.act = opt.next;
                    moveSelectedThumb(container)
                })
            })
        }
        if (nexttrans == 9) {
            nextli.css({
                "opacity": 1
            });
            opt.slots = opt.width / 20;
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            var ssamount = 0;
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                ssamount++;
                ss.transition({
                    "opacity": 0,
                    x: 0,
                    y: 0
                }, 0);
                ss.data("tout", setTimeout(function () {
                    ss.transition({
                        x: 0,
                        y: 0,
                        "opacity": 1
                    }, masterspeed)
                }, i * 4))
            });
            setTimeout(function () {
                removeSlots(container, opt);
                nextsh.find(".defaultimg").css({
                    "opacity": 1
                });
                if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                if ($.browser.msie && $.browser.version < 9) actsh.find(".defaultimg").css({
                    "opacity": 1
                });
                opt.act = opt.next;
                moveSelectedThumb(container)
            }, masterspeed + ssamount * 4)
        }
        if (nexttrans == 10) {
            nextli.css({
                "opacity": 1
            });
            opt.slots = opt.height / 20;
            prepareOneSlideV(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            var ssamount = 0;
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                ssamount++;
                ss.transition({
                    "opacity": 0,
                    x: 0,
                    y: 0
                }, 0);
                ss.data("tout", setTimeout(function () {
                    ss.transition({
                        x: 0,
                        y: 0,
                        "opacity": 1
                    }, masterspeed)
                }, i * 4))
            });
            setTimeout(function () {
                removeSlots(container, opt);
                nextsh.find(".defaultimg").css({
                    "opacity": 1
                });
                if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                if ($.browser.msie && $.browser.version < 9) actsh.find(".defaultimg").css({
                    "opacity": 1
                });
                opt.act = opt.next;
                moveSelectedThumb(container)
            }, masterspeed + ssamount * 4)
        }
        if (nexttrans == 11) {
            nextli.css({
                "opacity": 1
            });
            opt.slots = 1;
            prepareOneSlide(nextsh, opt, true);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            var ssamount = 0;
            nextsh.find(".slotslide").each(function (i) {
                var ss = $(this);
                ssamount++;
                if (opt.ie9) ss.transition({
                    "opacity": 0
                }, 0);
                else ss.transition({
                    "opacity": 0,
                    rotate: opt.rotate
                }, 0);
                ss.transition({
                    "opacity": 1,
                    rotate: 0
                }, masterspeed)
            });
            setTimeout(function () {
                removeSlots(container, opt);
                nextsh.find(".defaultimg").css({
                    "opacity": 1
                });
                if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                if ($.browser.msie && $.browser.version < 9) actsh.find(".defaultimg").css({
                    "opacity": 1
                });
                opt.act = opt.next;
                moveSelectedThumb(container)
            }, masterspeed)
        }
        if (nexttrans == 12 || nexttrans == 13 || nexttrans == 14 || nexttrans == 15) {
            masterspeed = masterspeed * 3;
            nextli.css({
                "opacity": 1
            });
            opt.slots = 1;
            prepareOneSlide(nextsh, opt, true);
            prepareOneSlide(actsh, opt, true);
            actsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            var oow = opt.width;
            var ooh = opt.height;
            if (opt.fullWidth == "on") {
                oow = opt.container.parent().width();
                ooh = opt.container.parent().height()
            }
            var ssn = nextsh.find(".slotslide");
            if (nexttrans == 12) if (opt.ie9) ssn.transition({
                "left": oow + "px"
            }, 0);
            else ssn.transition({
                "left": oow + "px",
                rotate: opt.rotate
            }, 0);
            else if (nexttrans == 15) if (opt.ie9) ssn.transition({
                "left": 0 - opt.width + "px"
            }, 0);
            else ssn.transition({
                "left": 0 - opt.width + "px",
                rotate: opt.rotate
            }, 0);
            else if (nexttrans == 13) if (opt.ie9) ssn.transition({
                "top": ooh + "px"
            }, 0);
            else ssn.transition({
                "top": ooh + "px",
                rotate: opt.rotate
            }, 0);
            else if (nexttrans == 14) if (opt.ie9) ssn.transition({
                "top": 0 - opt.height + "px"
            }, 0);
            else ssn.transition({
                "top": 0 - opt.height + "px",
                rotate: opt.rotate
            }, 0);
            ssn.transition({
                "left": "0px",
                "top": "0px",
                opacity: 1,
                rotate: 0
            }, masterspeed, function () {
                removeSlots(container, opt, 0);
                if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                nextsh.find(".defaultimg").css({
                    "opacity": 1
                });
                opt.act = opt.next;
                moveSelectedThumb(container)
            });
            var ssa = actsh.find(".slotslide");
            if (nexttrans == 12) ssa.transition({
                "left": 0 - oow + "px",
                opacity: 1,
                rotate: 0
            }, masterspeed);
            else if (nexttrans == 15) ssa.transition({
                "left": oow + "px",
                opacity: 1,
                rotate: 0
            }, masterspeed);
            else if (nexttrans == 13) ssa.transition({
                "top": 0 - ooh + "px",
                opacity: 1,
                rotate: 0
            }, masterspeed);
            else if (nexttrans == 14) ssa.transition({
                "top": ooh + "px",
                opacity: 1,
                rotate: 0
            }, masterspeed)
        }
        if (nexttrans == 16) {
            actli.css({
                "position": "absolute",
                "z-index": 20
            });
            nextli.css({
                "position": "absolute",
                "z-index": 15
            });
            actli.wrapInner('<div class="tp-half-one"></div>');
            actli.find(".tp-half-one").clone(true).appendTo(actli).addClass("tp-half-two");
            actli.find(".tp-half-two").removeClass("tp-half-one");
            actli.find(".tp-half-two").wrapInner('<div class="tp-offset"></div>');
            actli.find(".tp-half-one").css({
                "width": opt.width + "px",
                "height": opt.height / 2 + "px",
                "overflow": "hidden",
                "position": "absolute",
                "top": "0px",
                "left": "0px"
            });
            actli.find(".tp-half-two").css({
                "width": opt.width + "px",
                "height": opt.height / 2 + "px",
                "overflow": "hidden",
                "position": "absolute",
                "top": opt.height / 2 + "px",
                "left": "0px"
            });
            actli.find(".tp-half-two .tp-offset").css({
                "position": "absolute",
                "top": 0 - opt.height / 2 + "px",
                "left": "0px"
            });
            if (!$.support.transition) {
                actli.find(".tp-half-one").animate({
                    "top": 0 - opt.height / 2 + "px"
                }, {
                    duration: 500,
                    queue: false
                });
                actli.find(".tp-half-two").animate({
                    "top": opt.height + "px"
                }, {
                    duration: 500,
                    queue: false
                })
            } else {
                var ro1 = Math.round(Math.random() * 40 - 20);
                var ro2 = Math.round(Math.random() * 40 - 20);
                var sc1 = Math.random() * 1 + 1;
                var sc2 = Math.random() * 1 + 1;
                actli.find(".tp-half-one").transition({
                    opacity: 1,
                    scale: sc1,
                    rotate: ro1,
                    y: 0 - opt.height / 1.4 + "px"
                }, 800, "in");
                actli.find(".tp-half-two").transition({
                    opacity: 1,
                    scale: sc2,
                    rotate: ro2,
                    y: 0 + opt.height / 1.4 + "px"
                }, 800, "in");
                if (actli.html() != null) nextli.transition({
                    scale: 0.8,
                    x: opt.width * 0.1,
                    y: opt.height * 0.1,
                    rotate: ro1
                }, 0).transition({
                    rotate: 0,
                    scale: 1,
                    x: 0,
                    y: 0
                }, 600, "snap")
            }
            nextsh.find(".defaultimg").css({
                "opacity": 1
            });
            setTimeout(function () {
                actli.css({
                    "position": "absolute",
                    "z-index": 18
                });
                nextli.css({
                    "position": "absolute",
                    "z-index": 20
                });
                nextsh.find(".defaultimg").css({
                    "opacity": 1
                });
                actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                if (actli.find(".tp-half-one").length > 0) actli.find(".tp-half-one >img, .tp-half-one >div").unwrap();
                actli.find(".tp-half-two").remove();
                opt.transition = 0;
                opt.act = opt.next
            }, 800);
            nextli.css({
                "opacity": 1
            })
        }
        if (nexttrans == 17) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideV(actsh, opt, true);
            prepareOneSlideV(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.transition({
                    rotateY: 350,
                    rotateX: 40,
                    perspective: "1400px",
                    rotate: 0
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        top: 0,
                        left: 0,
                        scale: 1,
                        perspective: "150px",
                        rotate: 0,
                        rotateY: 0,
                        rotateX: 0
                    }, masterspeed * 2, function () {
                        if (j == opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 100)
            })
        }
        if (nexttrans == 18) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.transition({
                    rotateX: 10,
                    rotateY: 310,
                    perspective: "1400px",
                    rotate: 0,
                    opacity: 0
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        top: 0,
                        left: 0,
                        scale: 1,
                        perspective: "150px",
                        rotate: 0,
                        rotateY: 0,
                        rotateX: 0,
                        opacity: 1
                    }, masterspeed * 2, function () {
                        if (j == opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 100)
            })
        }
        if (nexttrans == 19) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlide(actsh, opt, true);
            prepareOneSlide(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                if (direction == 1) ss.transition({
                    left: 0,
                    top: 0 + opt.height / 2,
                    perspective: opt.height * 2,
                    rotate3d: "100, 0, 0, -90deg"
                }, 0);
                else ss.transition({
                    left: 0,
                    top: 0 - opt.height / 2,
                    perspective: opt.height * 2,
                    rotate3d: "100, 0, 0, 90deg"
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        top: 0,
                        perspective: opt.height * 2,
                        rotate3d: "0, 0, 0, 0deg"
                    }, masterspeed * 2, function () {
                        if (j == opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 100)
            });
            actsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.transition({
                    top: 0,
                    perspective: opt.height * 2,
                    rotate3d: "0, 0, 0, 0deg"
                }, 0);
                actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                setTimeout(function () {
                    if (direction == 1) ss.transition({
                        left: 0,
                        top: 0 - opt.height / 2,
                        perspective: opt.height,
                        rotate3d: "100, 0, 0, 90deg"
                    }, masterspeed * 1.5, function () {});
                    else ss.transition({
                        left: 0,
                        top: opt.height / 2,
                        perspective: opt.height,
                        rotate3d: "100, 0, 0, -90deg"
                    }, masterspeed * 1.5, function () {})
                }, j * 100)
            })
        }
        if (nexttrans == 20) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideV(actsh, opt, true);
            prepareOneSlideV(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                if (direction == 1) ss.transition({
                    scale: 0.8,
                    top: 0,
                    left: 0 - opt.width,
                    perspective: opt.width,
                    rotate3d: "2, 5, 0, 110deg"
                }, 0);
                else ss.transition({
                    scale: 0.8,
                    top: 0,
                    left: 0 + opt.width,
                    perspective: opt.width,
                    rotate3d: "2, 5, 0, -110deg"
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        scale: 0.8,
                        left: 0,
                        perspective: opt.width,
                        rotate3d: "1, 5, 0, 0deg"
                    }, masterspeed * 2, "ease").transition({
                        scale: 1
                    }, 200, "out", function () {
                        if (j == opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 100)
            });
            actsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.transition({
                    scale: 0.5,
                    left: 0,
                    perspective: 500,
                    rotate3d: "1, 5, 0, 5deg"
                }, 300, "in-out");
                actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                setTimeout(function () {
                    if (direction == 1) ss.transition({
                        top: 0,
                        left: opt.width / 2,
                        perspective: opt.width,
                        rotate3d: "0, -3, 0, 70deg",
                        opacity: 0
                    }, masterspeed * 2, "out", function () {});
                    else ss.transition({
                        top: 0,
                        left: 0 - opt.width / 2,
                        perspective: opt.width,
                        rotate3d: "0, -3, 0, -70deg",
                        opacity: 0
                    }, masterspeed * 2, "out", function () {})
                }, j * 100)
            })
        }
        if (nexttrans == 21) {
            masterspeed = masterspeed + 100;
            if (opt.slots > 10) opt.slots = 10;
            nextli.css({
                "opacity": 1
            });
            prepareOneSlideV(actsh, opt, true);
            prepareOneSlideV(nextsh, opt, false);
            nextsh.find(".defaultimg").css({
                "opacity": 0
            });
            nextsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                if (direction == 1) ss.transition({
                    top: 0,
                    left: 0 - opt.width / 2,
                    perspective: opt.width * 2,
                    rotate3d: "0, 100, 0, 90deg"
                }, 0);
                else ss.transition({
                    top: 0,
                    left: 0 + opt.width / 2,
                    perspective: opt.width * 2,
                    rotate3d: "0, 100, 0, -90deg"
                }, 0);
                setTimeout(function () {
                    ss.transition({
                        left: 0,
                        perspective: opt.width * 2,
                        rotate3d: "0, 0, 0, 0deg"
                    }, masterspeed * 2, function () {
                        if (j == opt.slots - 1) {
                            removeSlots(container, opt);
                            nextsh.find(".defaultimg").css({
                                "opacity": 1
                            });
                            if (nextli.index() != actli.index()) actsh.find(".defaultimg").css({
                                "opacity": 0
                            });
                            opt.act = opt.next;
                            moveSelectedThumb(container)
                        }
                    })
                }, j * 100)
            });
            actsh.find(".slotslide").each(function (j) {
                var ss = $(this);
                ss.transition({
                    left: 0,
                    perspective: opt.width * 2,
                    rotate3d: "0, 0, 0, 0deg"
                }, 0);
                actsh.find(".defaultimg").css({
                    "opacity": 0
                });
                setTimeout(function () {
                    if (direction == 1) ss.transition({
                        top: 0,
                        left: opt.width / 2,
                        perspective: opt.width,
                        rotate3d: "0, 1000, 0, -90deg"
                    }, masterspeed * 1.5, function () {});
                    else ss.transition({
                        top: 0,
                        left: 0 - opt.width / 2,
                        perspective: opt.width,
                        rotate3d: "0, 1000, 0, +90deg"
                    }, masterspeed * 1.5, function () {})
                }, j * 100)
            })
        }
        var data = {};
        data.slideIndex = opt.next + 1;
        container.trigger("revolution.slide.onchange", data);
        container.trigger("revolution.slide.onvideostop")
    }
    function removeTheCaptions(actli, opt) {
        actli.find(".caption").each(function (i) {
            var nextcaption = actli.find(".caption:eq(" + i + ")");
            nextcaption.stop(true, true);
            clearTimeout(nextcaption.data("timer"));
            var easetype = nextcaption.data("easing");
            easetype = "easeInOutSine";
            var ll = nextcaption.data("repx");
            var tt = nextcaption.data("repy");
            var oo = nextcaption.data("repo");
            var rot = nextcaption.data("rotate");
            var sca = nextcaption.data("scale");
            if (nextcaption.find("iframe").length > 0) {
                var par = nextcaption.find("iframe").parent();
                var iframe = par.html();
                setTimeout(function () {
                    nextcaption.find("iframe").remove();
                    par.append(iframe)
                }, nextcaption.data("speed"))
            }
            try {
                if (rot != undefined || sca != undefined) {
                    if (rot == undefined) rot = 0;
                    if (sca == undefined) sca = 1;
                    nextcaption.transition({
                        "rotate": rot,
                        "scale": sca,
                        "opacity": oo,
                        "left": ll + "px",
                        "top": tt + "px"
                    }, nextcaption.data("speed") + 10, function () {
                        nextcaption.removeClass("noFilterClass");
                        nextcaption.css({
                            "visibility": "hidden"
                        })
                    })
                } else nextcaption.animate({
                    "opacity": oo,
                    "left": ll + "px",
                    "top": tt + "px"
                }, {
                    duration: nextcaption.data("speed") + 10,
                    easing: easetype,
                    complete: function () {
                        nextcaption.removeClass("noFilterClass");
                        nextcaption.css({
                            "visibility": "hidden"
                        })
                    }
                })
            } catch (e) {}
        })
    }
    function onYouTubePlayerAPIReady() {}
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            bt.stop();
            opt.videoplaying = true;
            opt.videostartednow = 1
        } else {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            if (opt.conthover == 0) bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - opt.cd - 100,
                queue: false,
                easing: "linear"
            });
            opt.videoplaying = false;
            opt.videostoppednow = 1
        }
    }
    function onPlayerReady(event) {
        event.target.playVideo()
    }
    function addEvent(element, eventName, callback) {
        if (element.addEventListener) element.addEventListener(eventName, callback, false);
        else element.attachEvent(eventName, callback, false)
    }
    function vimeoready(player_id) {
        var froogaloop = $f(player_id);
        froogaloop.addEvent("ready", function (data) {});
        froogaloop.addEvent("play", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            bt.stop();
            opt.videoplaying = true
        });
        froogaloop.addEvent("finish", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            if (opt.conthover == 0) bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - opt.cd - 100,
                queue: false,
                easing: "linear"
            });
            opt.videoplaying = false;
            opt.videostartednow = 1
        });
        froogaloop.addEvent("pause", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            if (opt.conthover == 0) bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - opt.cd - 100,
                queue: false,
                easing: "linear"
            });
            opt.videoplaying = false;
            opt.videostoppednow = 1
        })
    }
    function vimeoready_auto(player_id) {
        var froogaloop = $f(player_id);
        froogaloop.addEvent("ready", function (data) {
            froogaloop.api("play")
        });
        froogaloop.addEvent("play", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            bt.stop();
            opt.videoplaying = true
        });
        froogaloop.addEvent("finish", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            if (opt.conthover == 0) bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - opt.cd - 100,
                queue: false,
                easing: "linear"
            });
            opt.videoplaying = false;
            opt.videostartednow = 1
        });
        froogaloop.addEvent("pause", function (data) {
            var bt = $("body").find(".tp-bannertimer");
            var opt = bt.data("opt");
            if (opt.conthover == 0) bt.animate({
                "width": "100%"
            }, {
                duration: opt.delay - opt.cd - 100,
                queue: false,
                easing: "linear"
            });
            opt.videoplaying = false;
            opt.videostoppednow = 1
        })
    }
    function animateTheCaptions(nextli, opt, actli) {
        nextli.find(".caption").each(function (i) {
            offsetx = opt.width / 2 - opt.startwidth / 2;
            if (opt.bh > 1) {
                opt.bw = 1;
                opt.bh = 1
            }
            if (opt.bw > 1) {
                opt.bw = 1;
                opt.bh = 1
            }
            var xbw = opt.bw;
            var xbh = opt.bh;
            var nextcaption = nextli.find(".caption:eq(" + i + ")");
            nextcaption.stop(true, true);
            if (nextcaption.hasClass("coloredbg")) offsetx = 0;
            if (offsetx < 0) offsetx = 0;
            var offsety = 0;
            clearTimeout(nextcaption.data("timer"));
            var frameID = "iframe" + Math.round(Math.random() * 1E3 + 1);
            if (nextcaption.find("iframe").length > 0) {
                var ifr = nextcaption.find("iframe");
                if (ifr.attr("src").toLowerCase().indexOf("youtube") >= 0) try {
                    ifr.attr("id", frameID);
                    var player;
                    if (nextcaption.data("autoplay") == true) player = new YT.Player(frameID, {
                        events: {
                            "onStateChange": onPlayerStateChange,
                            "onReady": onPlayerReady
                        }
                    });
                    else player = new YT.Player(frameID, {
                        events: {
                            "onStateChange": onPlayerStateChange
                        }
                    });
                    ifr.addClass("HasListener")
                } catch (e) {} else if (ifr.attr("src").toLowerCase().indexOf("vimeo") >= 0) if (!ifr.hasClass("HasListener")) {
                    ifr.addClass("HasListener");
                    ifr.attr("id", frameID);
                    var isrc = ifr.attr("src");
                    var queryParameters = {}, queryString = isrc,
                        re = /([^&=]+)=([^&]*)/g,
                        m;
                    while (m = re.exec(queryString)) queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
                    if (queryParameters["player_id"] != undefined) isrc = isrc.replace(queryParameters["player_id"], frameID);
                    else isrc = isrc + "&player_id=" + frameID;
                    try {
                        isrc = isrc.replace("api=0", "api=1")
                    } catch (e) {}
                    isrc = isrc + "&api=1";
                    ifr.attr("src", isrc);
                    var player = nextcaption.find("iframe")[0];
                    if (nextcaption.data("autoplay") == true) $f(player).addEvent("ready", vimeoready_auto);
                    else $f(player).addEvent("ready", vimeoready)
                }
            }
            if (nextcaption.hasClass("randomrotate") && (opt.ie || opt.ie9)) nextcaption.removeClass("randomrotate").addClass("sfb");
            nextcaption.removeClass("noFilterClass");
            var imw = 0;
            var imh = 0;
            if (nextcaption.find("img").length > 0) {
                var im = nextcaption.find("img");
                if (im.data("ww") == undefined) im.data("ww", im.width());
                if (im.data("hh") == undefined) im.data("hh", im.height());
                var ww = im.data("ww");
                var hh = im.data("hh");
                im.width(ww * opt.bw);
                im.height(hh * opt.bh);
                imw = im.width();
                imh = im.height()
            } else if (nextcaption.find("iframe").length > 0) {
                var im = nextcaption.find("iframe");
                if (nextcaption.data("ww") == undefined) nextcaption.data("ww", im.width());
                if (nextcaption.data("hh") == undefined) nextcaption.data("hh", im.height());
                var ww = nextcaption.data("ww");
                var hh = nextcaption.data("hh");
                var nc = nextcaption;
                if (nc.data("fsize") == undefined) nc.data("fsize", parseInt(nc.css("font-size"), 0) || 0);
                if (nc.data("pt") == undefined) nc.data("pt", parseInt(nc.css("paddingTop"), 0) || 0);
                if (nc.data("pb") == undefined) nc.data("pb", parseInt(nc.css("paddingBottom"), 0) || 0);
                if (nc.data("pl") == undefined) nc.data("pl", parseInt(nc.css("paddingLeft"), 0) || 0);
                if (nc.data("pr") == undefined) nc.data("pr", parseInt(nc.css("paddingRight"), 0) || 0);
                if (nc.data("mt") == undefined) nc.data("mt", parseInt(nc.css("marginTop"), 0) || 0);
                if (nc.data("mb") == undefined) nc.data("mb", parseInt(nc.css("marginBottom"), 0) || 0);
                if (nc.data("ml") == undefined) nc.data("ml", parseInt(nc.css("marginLeft"), 0) || 0);
                if (nc.data("mr") == undefined) nc.data("mr", parseInt(nc.css("marginRight"), 0) || 0);
                if (nc.data("bt") == undefined) nc.data("bt", parseInt(nc.css("borderTop"), 0) || 0);
                if (nc.data("bb") == undefined) nc.data("bb", parseInt(nc.css("borderBottom"), 0) || 0);
                if (nc.data("bl") == undefined) nc.data("bl", parseInt(nc.css("borderLeft"), 0) || 0);
                if (nc.data("br") == undefined) nc.data("br", parseInt(nc.css("borderRight"), 0) || 0);
                if (nc.data("lh") == undefined) nc.data("lh", parseInt(nc.css("lineHeight"), 0) || 0);
                var fvwidth = opt.width;
                var fvheight = opt.height;
                if (fvwidth > opt.startwidth) fvwidth = opt.startwidth;
                if (fvheight > opt.startheight) fvheight = opt.startheight;
                if (!nextcaption.hasClass("fullscreenvideo")) nextcaption.css({
                    "font-size": nc.data("fsize") * opt.bw + "px",
                    "padding-top": nc.data("pt") * opt.bh + "px",
                    "padding-bottom": nc.data("pb") * opt.bh + "px",
                    "padding-left": nc.data("pl") * opt.bw + "px",
                    "padding-right": nc.data("pr") * opt.bw + "px",
                    "margin-top": nc.data("mt") * opt.bh + "px",
                    "margin-bottom": nc.data("mb") * opt.bh + "px",
                    "margin-left": nc.data("ml") * opt.bw + "px",
                    "margin-right": nc.data("mr") * opt.bw + "px",
                    "border-top": nc.data("bt") * opt.bh + "px",
                    "border-bottom": nc.data("bb") * opt.bh + "px",
                    "border-left": nc.data("bl") * opt.bw + "px",
                    "border-right": nc.data("br") * opt.bw + "px",
                    "line-height": nc.data("lh") * opt.bh + "px",
                    "height": hh * opt.bh + "px",
                    "white-space": "nowrap"
                });
                else nextcaption.css({
                    "width": opt.startwidth * opt.bw,
                    "height": opt.startheight * opt.bh
                });
                im.width(ww * opt.bw);
                im.height(hh * opt.bh);
                imw = im.width();
                imh = im.height()
            } else {
                var nc = nextcaption;
                if (nc.data("fsize") == undefined) nc.data("fsize", parseInt(nc.css("font-size"), 0) || 0);
                if (nc.data("pt") == undefined) nc.data("pt", parseInt(nc.css("paddingTop"), 0) || 0);
                if (nc.data("pb") == undefined) nc.data("pb", parseInt(nc.css("paddingBottom"), 0) || 0);
                if (nc.data("pl") == undefined) nc.data("pl", parseInt(nc.css("paddingLeft"), 0) || 0);
                if (nc.data("pr") == undefined) nc.data("pr", parseInt(nc.css("paddingRight"), 0) || 0);
                if (nc.data("mt") == undefined) nc.data("mt", parseInt(nc.css("marginTop"), 0) || 0);
                if (nc.data("mb") == undefined) nc.data("mb", parseInt(nc.css("marginBottom"), 0) || 0);
                if (nc.data("ml") == undefined) nc.data("ml", parseInt(nc.css("marginLeft"), 0) || 0);
                if (nc.data("mr") == undefined) nc.data("mr", parseInt(nc.css("marginRight"), 0) || 0);
                if (nc.data("bt") == undefined) nc.data("bt", parseInt(nc.css("borderTop"), 0) || 0);
                if (nc.data("bb") == undefined) nc.data("bb", parseInt(nc.css("borderBottom"), 0) || 0);
                if (nc.data("bl") == undefined) nc.data("bl", parseInt(nc.css("borderLeft"), 0) || 0);
                if (nc.data("br") == undefined) nc.data("br", parseInt(nc.css("borderRight"), 0) || 0);
                if (nc.data("lh") == undefined) nc.data("lh", parseInt(nc.css("lineHeight"), 0) || 0);
                nextcaption.css({
                    "font-size": nc.data("fsize") * opt.bw + "px",
                    "padding-top": nc.data("pt") * opt.bh + "px",
                    "padding-bottom": nc.data("pb") * opt.bh + "px",
                    "padding-left": nc.data("pl") * opt.bw + "px",
                    "padding-right": nc.data("pr") * opt.bw + "px",
                    "margin-top": nc.data("mt") * opt.bh + "px",
                    "margin-bottom": nc.data("mb") * opt.bh + "px",
                    "margin-left": nc.data("ml") * opt.bw + "px",
                    "margin-right": nc.data("mr") * opt.bw + "px",
                    "border-top": nc.data("bt") * opt.bh + "px",
                    "border-bottom": nc.data("bb") * opt.bh + "px",
                    "border-left": nc.data("bl") * opt.bw + "px",
                    "border-right": nc.data("br") * opt.bw + "px",
                    "line-height": nc.data("lh") * opt.bh + "px",
                    "white-space": "nowrap"
                });
                imh = nextcaption.outerHeight(true);
                imw = nextcaption.outerWidth(true)
            }
            if (nextcaption.hasClass("fade")) nextcaption.css({
                "opacity": 0,
                "left": xbw * nextcaption.data("x") + offsetx + "px",
                "top": opt.bh * nextcaption.data("y") + "px"
            });
            if (nextcaption.hasClass("randomrotate")) {
                nextcaption.css({
                    "left": xbw * nextcaption.data("x") + offsetx + "px",
                    "top": xbh * nextcaption.data("y") + offsety + "px"
                });
                var sc = Math.random() * 2 + 1;
                var ro = Math.round(Math.random() * 200 - 100);
                var xx = Math.round(Math.random() * 200 - 100);
                var yy = Math.round(Math.random() * 200 - 100);
                nextcaption.data("repx", xx);
                nextcaption.data("repy", yy);
                nextcaption.data("repo", nextcaption.css("opacity"));
                nextcaption.data("rotate", ro);
                nextcaption.data("scale", sc);
                nextcaption.transition({
                    opacity: 0,
                    scale: sc,
                    rotate: ro,
                    x: xx,
                    y: yy,
                    duration: "0ms"
                })
            }
            if (nextcaption.hasClass("lfr")) nextcaption.css({
                "opacity": 1,
                "left": 15 + opt.width + "px",
                "top": opt.bh * nextcaption.data("y") + "px"
            });
            if (nextcaption.hasClass("lfl")) nextcaption.css({
                "opacity": 1,
                "left": -15 - imw + "px",
                "top": opt.bh * nextcaption.data("y") + "px"
            });
            if (nextcaption.hasClass("sfl")) nextcaption.css({
                "opacity": 0,
                "left": xbw * nextcaption.data("x") - 50 + offsetx + "px",
                "top": opt.bh * nextcaption.data("y") + "px"
            });
            if (nextcaption.hasClass("sfr")) nextcaption.css({
                "opacity": 0,
                "left": xbw * nextcaption.data("x") + 50 + offsetx + "px",
                "top": opt.bh * nextcaption.data("y") + "px"
            });
            if (nextcaption.hasClass("lft")) nextcaption.css({
                "opacity": 1,
                "left": xbw * nextcaption.data("x") + offsetx + "px",
                "top": -25 - imh + "px"
            });
            if (nextcaption.hasClass("lfb")) nextcaption.css({
                "opacity": 1,
                "left": xbw * nextcaption.data("x") + offsetx + "px",
                "top": 25 + opt.height + "px"
            });
            if (nextcaption.hasClass("sft")) nextcaption.css({
                "opacity": 0,
                "left": xbw * nextcaption.data("x") + offsetx + "px",
                "top": opt.bh * nextcaption.data("y") - 50 + "px"
            });
            if (nextcaption.hasClass("sfb")) nextcaption.css({
                "opacity": 0,
                "left": xbw * nextcaption.data("x") + offsetx + "px",
                "top": opt.bh * nextcaption.data("y") + 50 + "px"
            });
            nextcaption.data("timer", setTimeout(function () {
                nextcaption.css({
                    "visibility": "visible"
                });
                if (nextcaption.hasClass("fade")) {
                    nextcaption.data("repo", nextcaption.css("opacity"));
                    nextcaption.animate({
                        "opacity": 1
                    }, {
                        duration: nextcaption.data("speed")
                    });
                    if (opt.ie) nextcaption.addClass("noFilterClass")
                }
                if (nextcaption.hasClass("randomrotate")) {
                    nextcaption.transition({
                        opacity: 1,
                        scale: 1,
                        "left": xbw * nextcaption.data("x") + offsetx + "px",
                        "top": xbh * nextcaption.data("y") + offsety + "px",
                        rotate: 0,
                        x: 0,
                        y: 0,
                        duration: nextcaption.data("speed")
                    });
                    if (opt.ie) nextcaption.addClass("noFilterClass")
                }
                if (nextcaption.hasClass("lfr") || nextcaption.hasClass("lfl") || nextcaption.hasClass("sfr") || nextcaption.hasClass("sfl") || nextcaption.hasClass("lft") || nextcaption.hasClass("lfb") || nextcaption.hasClass("sft") || nextcaption.hasClass("sfb")) {
                    var easetype = nextcaption.data("easing");
                    if (easetype == undefined) easetype = "linear";
                    nextcaption.data("repx", nextcaption.position().left);
                    nextcaption.data("repy", nextcaption.position().top);
                    nextcaption.data("repo", nextcaption.css("opacity"));
                    nextcaption.animate({
                        "opacity": 1,
                        "left": xbw * nextcaption.data("x") + offsetx + "px",
                        "top": opt.bh * nextcaption.data("y") + "px"
                    }, {
                        duration: nextcaption.data("speed"),
                        easing: easetype
                    });
                    if (opt.ie) nextcaption.addClass("noFilterClass")
                }
            }, nextcaption.data("start")))
        })
    }
    function countDown(container, opt) {
        opt.cd = 0;
        opt.loop = 0;
        if (opt.stopAfterLoops != undefined && opt.stopAfterLoops > -1) opt.looptogo = opt.stopAfterLoops;
        else opt.looptogo = 9999999;
        if (opt.stopAtSlide != undefined && opt.stopAtSlide > -1) opt.lastslidetoshow = opt.stopAtSlide;
        else opt.lastslidetoshow = 999;
        if (opt.looptogo == 0) opt.stopLoop = "on";
        if (opt.slideamount > 1 && !(opt.stopAfterLoops == 0 && opt.stopAtSlide == 1)) {
            var bt = container.find(".tp-bannertimer");
            if (bt.length > 0) {
                bt.css({
                    "width": "0%"
                });
                bt.animate({
                    "width": "100%"
                }, {
                    duration: opt.delay - 100,
                    queue: false,
                    easing: "linear"
                })
            }
            bt.data("opt", opt);
            opt.cdint = setInterval(function () {
                if (container.data("conthover-changed") == 1) {
                    opt.conthover = container.data("conthover");
                    container.data("conthover-changed", 0)
                }
                if (opt.conthover != 1 && opt.videoplaying != true) opt.cd = opt.cd + 100;
                if (opt.videostartednow == 1) {
                    container.trigger("revolution.slide.onvideoplay");
                    opt.videostartednow = 0
                }
                if (opt.videostoppednow == 1) {
                    container.trigger("revolution.slide.onvideostop");
                    opt.videostoppednow = 0
                }
                if (opt.cd >= opt.delay) {
                    opt.cd = 0;
                    opt.act = opt.next;
                    opt.next = opt.next + 1;
                    if (opt.next > container.find(">ul >li").length - 1) {
                        opt.next = 0;
                        opt.looptogo = opt.looptogo - 1;
                        if (opt.loop <= 0) opt.stopLoop = "on"
                    }
                    if (opt.stopLoop == "on" && opt.next == opt.lastslidetoshow - 1) {
                        clearInterval(opt.cdint);
                        container.find(".tp-bannertimer").css({
                            "visibility": "hidden"
                        });
                        container.trigger("revolution.slide.onstop")
                    }
                    swapSlide(container, opt);
                    if (bt.length > 0) {
                        bt.css({
                            "width": "0%"
                        });
                        bt.animate({
                            "width": "100%"
                        }, {
                            duration: opt.delay - 100,
                            queue: false,
                            easing: "linear"
                        })
                    }
                }
            }, 100);
            container.hover(function () {
                opt.conthover = 1;
                if (opt.onHoverStop == "on") {
                    bt.stop();
                    container.trigger("revolution.slide.onpause")
                }
            }, function () {
                if (container.data("conthover") != 1) {
                    container.trigger("revolution.slide.onresume");
                    opt.conthover = 0;
                    if (opt.onHoverStop == "on" && opt.videoplaying != true) bt.animate({
                        "width": "100%"
                    }, {
                        duration: opt.delay - opt.cd - 100,
                        queue: false,
                        easing: "linear"
                    })
                }
            })
        }
    }
})(window.jQuery);
