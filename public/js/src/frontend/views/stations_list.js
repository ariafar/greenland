define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/items_list',
    'text!frontend/templates/stations_list.html',
    'frontend/views/item',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, ListView, ListTpl, ItemView, Labels) {
       
        var StationsListView = ListView.extend({

            hasMoreItems : true,
            events : {
                'click .second-navbar span'     : 'getNewsByType',
                'click .add-item'               : 'showForm'
            },
            render : function(){
                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels,
                    type    : this.options.type,
                    target  : this.options.target
                }));

                return this;
            },

            renderItem: function(model, index) {
                $(".table", this.$el).show();
                
                var item_view = new ItemView({
                    model       : model,
                    index       : index,
                    fields      : this.selectedFields,
                    tagName     : "tr"
                });
                this.items.push(item_view);
                $(".items-wrapper tbody", this.$el).append(item_view.render().$el);
                item_view.$el.attr("id" , model.get("id"));
                item_view.afterRender();
            },

            showForm: function(){
                var rout = (this.collection.filters.get("type") == 1) ? "#add/offices" : "#add/stations";
                Backbone.history.navigate(rout, true);
            },
            
            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("type");

                if(type == 1){
                    $("#office_header", this.$el).show();
                    $("#station_header", this.$el).hide();
                }else{
                    $("#office_header", this.$el).hide();
                    $("#station_header", this.$el).show();
                }
                    
                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("type", type);
                }

            }

        });


        return StationsListView
    });
