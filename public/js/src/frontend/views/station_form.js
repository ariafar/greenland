define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'base/views/base_view',
    'frontend/models/item',
    'text!frontend/templates/station_form.html',
    'modules/map/views/config',
    'i18n!nls/labels',
    "jquery-helper",
    ], function($, Backbone, _, App, BaseView, ItemModel, FormTpl, MapConfigView, Labels) {
        var BoorsaneFormView = BaseView.extend({

            className : 'station-form-wrapper',

            events : {
                'click .save'               : 'save'
            },
                
            initialize: function(opt) {
                var $this = this;
                
                if(this.model.get("id")){
                    this.model.fetch({
                        async : true,
                        success : function(model){
                            $this.render()
                        },
                        error : function(){
                        }
                    })
                }else{
                    this.is_New = true;
                    this.render();
                }
            },

            render : function(){
                var $this = this,
                tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));
                
                $("#main-container").html(this.$el);

                this.mapView = new MapConfigView({
                    model                       : this.model,
                    onSelectPositionCallback    : function(location){
                        $("[name=latitude]", $this.$el).val(location.lat());
                        $("[name=longitude]", $this.$el).val(location.lng());
                    }
                });

                this.mapView.registerEvents();
            },

            initMap : function(){
                var location = [this.model.get('latitude'), this.model.get('longitude')];
                $(".gmap3", this.$el).gmap3({
                    marker:{
                        latLng: location,
                        options:{
                            draggable:false
                        }
                    },
                    map:{
                        options:{
                            center : location,
                            zoom: 15,
                            mapTypeControl : true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                            },
                            navigationControl: true,
                            scrollwheel: true,
                            streetViewControl: true,
                            zoomControl: true,
                            zoomControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP,
                                style: "SMALL"
                            },
                            panControl: false,
                            panControlOptions: {
                                position: google.maps.ControlPosition.RIGHT_TOP,
                                style: "SMALL"
                            }
                        }
                    }
                });
            },

            changePart : function(){
                var content =  $("#content", this.$el).val();
                this.model.set(this.part, content);
                $("#content", this.$el).val("");
                $(".images-list", this.$el).empty();
                this.part = $(".boursane-parts", this.$el).val();
                if(!this.images){
                    this.images = [];
                }

                this.loadData();
                
            },
            
            save : function(){
                var $this = this;
                var data = $("form", this.$el).serializeJSON();

                this.model.save(_.extend(data, {
                    images : $this.images
                }), {
                    success : function(){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                            window.history.back();
                        }, 2000);
                    //                        if($this.is_New) {
                    //                            var target = $this.model.get("target");
                    //                            var type = $this.model.get("type");
                    //                            $this.model = new ItemModel({
                    //                                target : target,
                    //                                type    : type
                    //                            });
                    //                            window.history.back();
                    //                        //                            $this.render();
                    //                        //                            $this.delegateEvents();
                    //                        }
                      
                    }
                })
              
            }

        })

        return BoorsaneFormView;
    
   
    });




