define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'frontend/views/item_form',
    'text!frontend/templates/market_form.html',
    'modules/map/views/config',
    'i18n!nls/labels',

    ], function($, Backbone, _, App, ItemFormView, FormTpl, MapConfigView, Labels) {
        
        var MarketFormView = ItemFormView.extend({

            initialize: function(opt) {
                var $this = this;
                this.part = "indicesChange";
                this.images = [];

                this.parts = ["indicesChange", "turnover", "maxOrder", "coinCurrencyMarket", "globalMarket", "news" , "stockMarketGroups", "commodityExchange", "futuresExchange" ];

                if(this.model.get("id")){
                    this.render()
                    if($this.model.get("photos") && $this.model.get("photos").length > 0){
                        _.each($this.model.get("photos"), function(photo){
                            $this.addPhoto(photo)
                        })
                    }
                }else{
                    this.is_New = true;
                    this.render();
                }
            },

            render : function(){
                var tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));

                this.mapView = new MapConfigView({
                    model : this.model
                });
                return this;
            },

            afterRender : function(){
                this.initUploader();
                this.mapView.registerEvents();
            },
            
            save : function(){
                if(!this.validate())
                    return;

                $(".error").removeClass("error");
                
                var $this = this,
                data = _.extend( $("form", this.$el).serializeJSON(), {
                    images : $this.images
                });

                if(this.mapView && this.mapView.location){
                    data['latitude'] = this.mapView.location.lat();
                    data['longitude'] = this.mapView.location.lng();
                }

                this.model.save(_.extend(data,{
                    parentId    : (!this.collection.filters.get("parentId")) ? 0 : this.collection.filters.get("parentId")
                }), {
                    success : function(model){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                        }, 2000);
                        $this.collection.add(model);
                        $("#new-item-modal").modal("hide");
                    }
                })
            }
        })

        return MarketFormView;
    
   
    });




