
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'frontend/models/item',
    'frontend/models/response',
    'text!frontend/templates/evaluation_preview.html',
    'text!frontend/templates/question.html',
    'text!frontend/templates/answer_details.html',
    'chart',
    'i18n!nls/labels',
    ], function ($, _, Backbone, BaseView, EvaluationModel, ResponseModel, Tpl, QuestionTpl, AnswerDetialsTpl, Chart, Labels) {

        var ItemView = BaseView.extend({

            events : {

            },

            initialize : function(options) {
                var $this = this;

                this.template = _.template(Tpl);
                this.avaluation = new EvaluationModel({
                    id : options.evalutionId,
                    target : "evaluations"
                });

                this.avaluation.fetch({
                    success : function(){
                        $this.render();
                    }
                });
            },

            render : function(fields) {
                this.$el.html(this.template(_.extend(this.avaluation.toJSON(), {
                    labels  : Labels
                })));

                var response = this.avaluation.get("response");
                this.afterRender(response);
            },

            afterRender : function(items){
                var $this = this;

                _.each(items, function(item){
                    var q_id = item.questionId;
                    var el = $(".questions-list .item-wrapper." + q_id);
                    var tpl = _.template(AnswerDetialsTpl);
                    $(".answer-perecentage", el).html(tpl({
                        labels  : Labels,
                        items : item.items
                    }));
                    $this.initChart($("#chart_" + q_id), item.items);
                })

            },

            initChart : function(el, items){
                var colors = ["red", "blue", "green", "yellow", "purple", "brown"],
                data = [];
                _.each(items, function(item, index){
                    data.push({
                        color : colors[index],
                        value : Math.round(item.percent , 2) * 100,
                        label : Labels['option_' + (index + 1)]
                    });
                })

                var ctx = el.get(0).getContext("2d");
                new Chart(ctx).Pie(data, {
                    segmentShowStroke  : false,
                    tooltipTitleFontSize : 10
                });
            }

        });

        return ItemView;
    });
