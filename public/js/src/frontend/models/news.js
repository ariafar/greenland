define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var NewsModel = ItemModel.extend({

            defaults : {
                'id'              : null,
                'photoId'         : null,
                'title'           : null,
                'shortDesc'       : null,
                'longDesc'        : null,
                'photos'          : []
            },
            url : function() {
                return '/api/news/'+ (this.get('id') || '') + "/?deletedImage=0&deleted=0";;
            }
            
        });

        return NewsModel;
    });
