define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
], function ($, Backbone, _, ItemModel) {

    var MarketModel = ItemModel.extend({

        defaults : {
            'id' : null,
            'parentId' : null,
            'marketCode' : null,
            'name' : null,
            'establishDate' : null,
            'activityType' : null,
            'boothCount' : null,
            'conexCount' : null,
            'kioskCount' : null,
            'placeCount' : null,
            'locationCount' : null,
            'frankishCount' : null,
            'customerCount' : null,
            'parking' : null,
            'tell' : null,
            'fax' : null,
            'isActive' : 1,
            'address' : null,
            'latitude' : null,
            'longitude' : null,
            'creationDate' : null,
            'updateDate' : null,
            'deleted' : 0,
            'photos' : [],
            'target'    : "markets",

        }
            
    });

    return MarketModel;
});
