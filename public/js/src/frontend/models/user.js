define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
], function ($, Backbone, _, ItemModel) {

    var UserModel = ItemModel.extend({

        defaults : {
            'id' : null,
            'name' : null,
            'degree' : null,
            'experiences' : null,
            'position' : null,
            'managerMessage' : null,
            'type' : null,
            'emailAddress' : null,
            'telNumber' : null,
            'creationDate' : null,
            'updateDate' : null,
            'deleted' : 0,
            'photoId' : null,
            'photo' : null,
            'startDate' : null,
            'endDate'  : null
        }
    });

    return UserModel;
});
