define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
], function ($, Backbone, _, ItemModel) {

    var ProjectModel = ItemModel.extend({

        defaults : {
            'id' : null,
            'name' : null,
            'aims' : null,
            'actions' : null,
            'openingDate' : null,
            'progress' : null,
            'location' : null,
            'creationDate' : null,
            'updateDate' : null,
            'deleted' : 0,
            'photos' : []
        }
            
    });

    return ProjectModel;
});
