define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var StationModel = ItemModel.extend({

            defaults : {
                'id' : null,
                'type' : null,
                'code' : null,
                'areaCode' : null,
                'regionCode' : null,
                'address' : null,
                'telNumberOne' : '',
                'mobile' : '',
                'fax' : '',
                'managerName' : '',
                'telNumberTwo' : '',
                'latitude' : null,
                'longitude' : null,
                'creationDate' : null,
                'updateDate' : null,
                'deleted' : 0

            }
            
        });

        return StationModel;
    });
