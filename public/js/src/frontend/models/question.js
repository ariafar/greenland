define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var QModel = Backbone.Model.extend({
            defaults : {
                id              : null
            },
            
            url : function() {
                return '/api/evaluations/'+ this.get('evaluationId') +'/questions/' + (this.get('id') || "");
            }
            
        });

        return QModel;
    });
