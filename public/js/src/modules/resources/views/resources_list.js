define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'modules/resources/views/resources_list_item',
    'libs/jqueryPlugins/jquery.flexslider-min',
    'text!modules/resources/templates/resources_list.html',
    'i18n!modules/resources/nls/labels'
    ], function ($, _, Backbone, BaseListView, ListItemView, FlexSlider, resourcesAdminListTpl,labels) {
        var resourcesAdminListView = BaseListView.extend({
            tagName    : 'ul',
            className  : 'sites',
            events : {},

            initialize : function() {
                var self = this;

                this.template = _.template(resourcesAdminListTpl, {
                    labels : labels
                });
                resourcesAdminListView.__super__.initialize.apply(this, arguments);  
                
                this.collection.on('fetchSuccess', function(){
                    self.render();
//                    new HeaderView({
//                        HeaderTpl : HeaderTpl
//                    });
                })
            },

            render    : function() {
                this.$el.html(this.template);
                $('.resources-list').append(
                    '<li class="resources photo-list-item-thumb" style="height:250px;">'+
                    '<form>'+
                    '<div id="uploader">'+
                    '<p>You browser doesnt have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>'+
                    '</div>'+
                    '</form>'+
                    '</li>'
                    );
                
                for (var i = 0; i < this.collection.length; i++) {
                    this.renderItem(this.collection.models[i]);
                }
                this.trigger('afterRender');
                return this;
            },

            renderItem: function(model) {
                var item = new ListItemView({
                    "model": model
                });

                item.render().$el.appendTo($('.resources-list'));
            }
        });

        return resourcesAdminListView;
    });
