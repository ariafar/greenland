define([
    'jquery',
    'underscore',
    'backbone',
    'modules/users/views/login',
    'modules/houses/collections/houses',
    'modules/houses/views/houses_list',
    'modules/users/views/users_list',
    
    ], function ($, _, Backbone , LoginView, HousesCollection, HousesListView, UsersListView ) {
        var userRouter = Backbone.Router.extend({
            routerViews  : [],
            routesUrl    : [],
            
            routes : {
                ''                  : 'listUsers',
                'signup'            : 'signup',
                'login'             : 'login',
                'chzngePassword'    : 'changePassword',
                'chargeService'     : 'chargeService',
                'zooncan'           : 'myZooncan'
            },
                    

            before : function(){
                if(window.currentUser && window.currentUser.get('id'))
                    $('body').addClass('bg');
                else
                    $('body').removeClass('bg');
            },

            listUsers : function() {
                var usersList = new UsersListView();
                var container = $("#main-container");
                container.html(usersList.render().$el);
            },

            changePassword : function() {
                require(['modules/users/views/edit_profile'], function(EditProfileView) {
                    var editProfile = new EditProfileView({
                        type    : 'changePassword'
                    });
                    $("#main-container").html(editProfile.$el);
                //                    editProfile.passwordComplexify();
                });
            },

            chargeService : function(){
                require(['modules/users/views/charge_service'], function(ChargeServiceView) {
                    var chargeService = new ChargeServiceView();
                    $("#main-container").html(chargeService.$el);
                    chargeService.afterRender();
                });
            },

            signup : function() {
                var $this = this;
                require(['modules/users/views/sign_up'], function(sign_upView) {
                    var sign_upView = new sign_upView();
                    $('#main-container').html(sign_upView.$el);
                    sign_upView.registerEvents();
                });
            },
            
            login : function() {
                var $this = this;
                var loginView = new LoginView();
                $('#main-container').html(loginView.$el);
            },

            myZooncan : function(){

                var container = $("#main-container");
                container.empty();

                var zooncan_collection = new HousesCollection([]);
                this.housesList = new HousesListView({
                    url     : "/zooncans/houses",
                    collection : zooncan_collection,
                    zooncans    : true,
                    type        : 'GET'
                });

                container.html(this.housesList.$el);
                this.housesList.hideFilter();
            }
                    
                    
            

        });

        return userRouter;
    });
