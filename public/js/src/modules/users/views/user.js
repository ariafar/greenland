define([
    'jquery',
    'backbone',
    'underscore'
], function($, Backbone, _) {
    var userView = Backbone.View.extend({
        el : '#dasa',
        render : function(event) {
            var compiled_tpl = _.template($("#profile-template").html());
            this.$el.html(compiled_tpl(this.model.toJSON()));

            return this;
        }
    });

    return userView;
});
