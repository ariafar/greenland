define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'libs/jquery-helper',   
    'modules/users/models/user',
    'text!modules/users/templates/login_popup.html',
    'modules/users/views/login'
    ], function ($, Backbone, _, App, jqHelper, userModel, loginTmp, LoginView) {

        var PopupLoginView = LoginView.extend({
            
            className : 'modal fade login-popup',

            initialize : function() {
                this.template = _.template(loginTmp);
                this.render();
            },

            render : function() {                          
                this.$el.html(this.template());
                this.$el.modal('show');
                this.registerEvents();
                App.processScroll();  
                return this;
            }
            
        });

        return PopupLoginView;
    });
