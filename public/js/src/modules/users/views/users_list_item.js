define([
    'jquery',
    'underscore',
    'backbone',
    'modules/users/models/service',
    'base/views/item_view',
    'text!modules/users/templates/users_list_item.html',
    'text!modules/users/templates/balance_popup.html',
    'i18n!modules/users/nls/labels'
    ], function ($, _, Backbone, ServiceModel, BaseItemView,ItemTpl , BalancePopup, labels) {
        
        var UserListItemView = BaseItemView.extend({
            className : 'user_item row row-fluid show-grid',
            
            initialize : function() {   
                this.template = _.template(ItemTpl);
                this.model.on('change', this.render);
            },
        
            events : 
            {               
                'click      .increase-balance'     : 'increaseBalance' ,
            },
            
            render    : function() {                     
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });

                this.serviceModel = new ServiceModel( (this.model.get('service') || {}) );
                 
                this.$el.html(this.template(obj));   
                $('[rel=tooltip]', this.$el).tooltip();         
                return this;
            },    
            increaseBalance : function(){
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });
                var $this = this;
                var balancePopup =  _.template(BalancePopup);
                
                $('#balance-modal').html(balancePopup(obj));
                $('#balance-modal').modal('show');


                 $('#balance-modal [name=balance]').on("keyup", function(e){
//                     debugger
                     $this.setVal(e);
                 })


                $('#balance-modal .increse').click(function(){
                    var el = $('input[name=balance]');
                    if(el.hasClass('error'))
                        return;
                    else{
                        var balance = $this.getNumber(el.val().trim());
                        if(!parseInt(balance) || parseInt(balance).toString().length != balance.length){
                            el.val("");
                            return;
                        }
                        var old_balance = $this.serviceModel.get('balanc') || 0 ;
                        
                        $this.serviceModel.save({
                            balanc  : parseInt(old_balance) + parseInt(balance),
                            userId  : $this.model.get('id')
                        }, {
                            success : function(model){
                                $('#balance-modal').modal('hide');
                                $this.model.set('service', model.toJSON(),{
                                    silent : true
                                })
                            }
                        })
                    }

                })

            }

        });
        
        return UserListItemView;
    });