define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'modules/images/views/image_item',
    ], function ($, _, Backbone, BaseListView ,ImageItemView) {
        
        var resourcesAdminListView = BaseListView.extend({
            tagName  : 'ul',
            className : 'photos-list',
            events : {
            },
            initialize : function()
            {
                return;
                this.collection.bind('add', function(model, collection) {
                    this.renderItem(model ,collection);
                }, this);
                this.render();
            },
            render    : function() {
                var $this = this;
                
                this.collection.each(function(model){
                    $this.renderItem(model ,$this.collection);
                })
            },
            renderItem: function(model, collection) {
                var item = new ImageItemView({
                    model : model,
                    collection  : collection
                });
                this.$el.append(item.render().$el);
            } ,
            afterRenderItems : function(){  
                var self = this;                  
                var options = {                  
                    container: self.$el, 
                    offset: 50,
                    itemWidth: 190,
                    autoResize: true
                };
                $('li.resources' , this.$el).wookmark(options);
                $('.list-resources a.resource-list-item').lightBox();
            }
        });

        return resourcesAdminListView;
    });
