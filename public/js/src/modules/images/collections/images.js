define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'modules/images/models/image'
    ], function ($,_, Backbone, BaseCollection, ImageModel) {
        var ImagesCollection = BaseCollection.extend({
            
            model : ImageModel,
        
            initialize : function(models, opt){
                this.url = ( opt && opt.url ) ? opt.url : '/api/albums';
                
                ImagesCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
            url   : function(){
                return this.url;
            },
            
            parse  : function(resp){
                
                if(resp && this.filters){
                    this.filters.count = resp.count ;
                } 
                return resp.list;
            }
        });

        return ImagesCollection;
    });