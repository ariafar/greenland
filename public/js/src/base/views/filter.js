define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'modules/sites/views/new_site',
    'base/model/FilterModel',
    'i18n!modules/sites/nls/labels'
    ], function($, Backbone, _, App, newSiteView ,filterModel , labels) {
        
        var FilterView = App.BaseView.extend({      

            events : {
                'click .search'             : 'search',
                'click .reset-filter'       : 'resetFilter',
                'keyup .search-text'        : 'searchByEnter',
                'click .age li'             : 'search',
                'click .sort-options li'    : 'sort'                
            },
            
            render    : function() {
                this.$el.html(_.template(this.options.template, {
                    labels : labels
                }));
                if($( ".category-site" ).length > 0) this.registerEvents();
                return this;
            },  
            
            searchByEnter : function(e){
                if(e.keyCode == 13){                      
                    this.search(e);                   
                }
                e.stopPropagation();
                return false;
            },
            
            resetFilter : function(){                
                this.options.callee.collection.filters = new filterModel();
                $('.reset-filter').closest('.navbar').find('input').val('');
                $($('.reset-filter').closest('.navbar').find('select option')[0]).attr('selected', 'selected');
                var sortOption = $($('.sort-drop-down-toggle').next().find('li a')[0]).text();
                $('.sort-drop-down-toggle .btn-order-label').text(sortOption);
                this.options.callee.fetchCollection()
            },
            
            search : function(e){                
                var el = $(e.target);
                var searchInput =  $('.search-text');
                var searchObj = this.collection.filters.get('options') ? this.collection.filters.get('options') : {};
                (searchInput.data('type') == 'title') ?  searchObj.title = searchInput.val() : searchObj.name = searchInput.val();
                
                this.collection.filters.set('options' , searchObj);                    
                this.collection.filters.set('start' , 0);                    
                if(el.data('type') == 'age'){
                    
                    var age  = {
                        from :  el.data('from'),
                        to   :  el.data('to')
                    }
            
                    this.collection.filters.set('options' , {
                        age : age
                    })  
                }
                this.options.callee.fetchCollection();
                window.scrollTo(0, 0);
                return false;
            }, 
            
            sort : function(e) {
                var searchObj = this.collection.filters.get('options') ? this.collection.filters.get('options') : {};
                searchInput =  $('.search-text', this.$el);
                
                (searchInput.data('type') == 'title') ?  searchObj.title = searchInput.val() : searchObj.name = searchInput.val();
                
                this.collection.filters.set('options' , searchObj); 
                
                this.collection.filters.set({
                    'order_by': $(e.target).data('value')
                });                
                this.options.callee.fetchCollection();
                $(e.currentTarget.parentNode).prev().find('.btn-order-label').html(e.target.innerHTML);
                window.scrollTo(0, 0);
            },
            
            categorySelect : function(e){                
                var cat_id = $('#category-filter' +  " option[value='" + $(e.target).val() + "']").data('catid');   
                this.collection.filters.set({
                    options : {
                        'category_id' : cat_id
                    }
                });
                this.collection.reset();
                $('.sites-list').empty();
                this.collection.fetch();                       
                window.scrollTo(0, 0);
            },
            
            registerEvents : function(){                
                $('#category-filter').on('change', function(e){
                    self.categorySelect(e);
                })
                
                var self = this;                
                $.ajax({
                    url  : "api/sites/categories",
                    dataType : 'json',         
                    success: function( data ){                        
                        for(var i in data){                     
                            $('#category-filter').append('<option data-catid="' + data[i].id + '" value="' + data[i].title + '"' + '>' + data[i].title + '</option>');
                        }                       
                    }
                });
                
            //                $( ".category-site" ).autocomplete({
            //                    selectFirst: true,
            //                    autoFocus: true,
            //                    source: function(request, response) {                        
            //                        var s = [];
            //                        $.ajax({
            //                            url  : "api/sites/categories",
            //                            dataType : 'json',
            //                            data : {
            //                                'title'  : request.term,
            //                                'query'  : 'full'   
            //                            },
            //
            //                            success: function( data ) {
            //                                for(var i in data){
            //                                    var cat = {
            //                                        label : data[i].title,
            //                                        value : data[i].id
            //                                    }
            //                                    s.push(cat);
            //                                }
            //
            //                                response(s, request.term );
            //                            }
            //                        });
            //
            //                    },
            //                    minLength: 0,
            //                    select: function( event, ui ) {
            //                        $( ".category-site" ).val(ui.item.label);
            //                        
            //                        self.collection.filters.set({
            //                            options : {
            //                                'category_id' : ui.item.value
            //                            }
            //                        });
            //                       
            //                        self.collection.reset();
            //                        $('.sites-list').empty();
            //                        self.collection.fetch();                       
            //                        window.scrollTo(0, 0);
            //                        return false;
            //                    },
            //                    focus: function(event, ui) {
            //                        $( ".category-site" ).val(ui.item.label);
            //                        return false;
            //                    }
            //
            //                });

            }
        });

        return FilterView;
    });
