(function($) {
    $.fn.toggleSlide = function(options) {
        var el =  $(this.selector);
        var distance = $("#sidr").width() -15 ;
        el.click(function(){
            
            if($(el).hasClass('show-slide')){
                $(el).removeClass('show-slide');
                $(el).addClass('hide-slide');

                $("#sidr").animate({
                    right:'+=' + distance + 'px'
                },200)
                //{queue:false, duration:300, easing: 'swing'}
                
                $("#main-content").animate({
                    'margin-right':'+=' + distance + 'px'
                },200);
                
            }

            else{
                $(el).removeClass('hide-slide');
                $(el).addClass('show-slide');

                $("#sidr").animate({
                    right:'-=' + distance + 'px'
                },200)


                $("#main-content").animate({
                    'margin-right':'-=' + distance + 'px'
                },200);
            }
        });
    }
})(jQuery);