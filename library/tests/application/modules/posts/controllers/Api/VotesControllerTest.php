<?php

class Posts_Api_VotesControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');

        $user = $this->createTestUser();
        $post = $this->createTestPost($user);

        $this->dispatch('/api/posts/' . $post->getId() . '/votes');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_votes');
        $this->assertAction('index');
        return $post->getId();
    }

    /**
     * @depends testIndexAction
     */
    public function testPostAction($postId)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);



        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'post_id'  => $postId,
                    'type'     => 1,
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $postId . '/votes');
        $this->assertResponseCode(200);
        $result    = $this->response->getBody();
        $voteArray = json_decode($result, true);
        $this->assertEquals($voteArray['post_id'], $postId);
        $this->assertEquals($voteArray['voter_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($voteArray['type'], 1);

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('api/posts/' . $postId . '/votes');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_votes');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $voteArray['post_id'];
    }

}

?>
