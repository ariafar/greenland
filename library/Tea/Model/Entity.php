<?php

class Tea_Model_Entity
{

    protected $_new = true;
    protected $_properties;

    public function __toString()
    {
        $str = " (object): {";
        foreach ($this->_properties as $key => $value) {
            if (is_array($value)) {
                $valuestr = " (array): {";
                foreach ($value as $i => $data) {
                    $valuestr .= $i . " :: " . $data . ",\t";
                }
                $valuestr .= "}";
            } else {
                $valuestr = $value;
            }
            $str .= $key . " : " . $valuestr . ",\t";
        }
        return $str . "} ";
    }

    public function toArray($forInsert = false)
    {
        $properties = array();

        foreach ($this->_properties as $key => $value) {
            $func = str_replace('_', ' ', $key);
            $func = 'get' . str_replace(' ', '', ucwords($func));
            $value = $this->$func($forInsert);
            $properties[$key] = $this->_propertyToArray($value);
        }

        return $properties;
    }

    private function _propertyToArray($value)
    {
        $property = null;

        if (is_object($value)) {
            if (method_exists($value, 'toArray')) {
                $property = $value->toArray();
            } elseif ($value instanceof DateTime) {
                $property = $value->format('Y-m-d');
            } else {
                $property = (string) $value;
            }
        } elseif (is_array($value)) {
            $array = array();
            foreach ($value as $key => $data) {
                $array[$key] = $this->_propertyToArray($data);
            }
            $property = $array;
        } else {
            $property = $value;
        }

        return $property;
    }

    public function getProperties()
    {
        return $this->_properties;
    }

    public function __construct()
    {
        return $this;
    }

    public function __call($name, $arguments)
    {
        if (strlen($name) > 3) {
            if (substr($name, 0, 3) == 'set') {
                $name = substr($name, 3);
                $name = lcfirst($name);

                if (array_key_exists($name, $this->_properties)) {
                    $this->_properties[$name] = $arguments[0];
                }
            } elseif (substr($name, 0, 3) == 'get') {
                $name = substr($name, 3);
                $name = lcfirst($name);

                if (array_key_exists($name, $this->_properties)) {
                    return $this->_properties[$name];
                }
            }
        }
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return     true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->_new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     *
     * @param boolean $b the state of the object.
     *
     * @return void
     */
    public function setNew($b)
    {
        $this->_new = (boolean) $b;
    }

   /**
     * Convert Datetime to GMT Unix Timestamp
     *
     * @param DateTime $datetime DatetTime object
     *
     * @return int unix timestamp
     */
    public static function getGmtTimestamp($datetime)
    {
        if ($datetime == null) {
            return null;
        }

        if (is_string($datetime) && preg_match('/^[0-9]+$/', $datetime)) {
            $datetime = "@" . $datetime;
        }

        if (!$datetime instanceof DateTime) {
            try {
                $datetime = new DateTime($datetime);
            } catch (Exception $e) {
                $datetime = new DateTime('now');
            }
        }

        return (int)gmdate('U', $datetime->format('U'));
    }

    /**
     * conver Gmt Timestamp to DateTime object
     *
     * @param int $gmt_timestamp unix timestamp
     *
     * @return DateTime
     */
    public static function getDateTime($gmt_timestamp)
    {
        if ($gmt_timestamp == null) {
            return null;
        }
        date_default_timezone_set('Asia/Tehran');
        $dt = new DateTime();
        @$dt->setTimestamp($gmt_timestamp);
        return $dt;
    }

    public static function getGmtDateTime($str)
    {
        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));
        return $dt;
        return date_create($str, new DateTimeZone('UTC'));
    }

    public static function getLocalDateTime($str)
    {
        if ($str instanceof DateTime) {
            return $str;
        }
        $date = date_create($str, new DateTimeZone('Asia/Tehran'));
        $date->setTimeZone(new DateTimeZone(date_default_timezone_get()));
        return $date;
    }

    /**
     * Convert Gate to Datet
     *
     * @param Age $age int object
     *
     * @return Date
     */
    public static function convertAgeToDate($age)
    {
        $nowDate  = Tea_Model_Entity::getDateTime('now');
        $nowYear  = (int)$nowDate->format('Y');
        $nowMonth = $nowDate->format('m');
        $nowDay   = $nowDate->format('d');

        $bornYear = (string)($nowYear - $age);
        $bornDate = $bornYear . '-' . $nowMonth . '-' . $nowDay;
        return $bornDate;
    }

    public function getCreationDate($forInsert=false)
    {
        if ($forInsert) {
            return $this->_properties['creationDate'];
        }

        return $this->getLocalDateTime($this->_properties['creationDate'])->format('Y-m-d H:i:s');
    }

    public function setCreationDate($str)
    {
        $this->_properties['creationDate'] = $this->getGmtDateTime($str)->format('Y-m-d H:i:s');
    }

    public function getUpdateDate($forInsert=false)
    {
        if ($forInsert) {
            return $this->_properties['updateDate'];
        }

        return $this->getLocalDateTime($this->_properties['updateDate'])->format('Y-m-d H:i:s');
    }

    public function setUpdateDate($str)
    {
        $this->_properties['updateDate'] = $this->getGmtDateTime($str)->format('Y-m-d H:i:s');
    }
}
