<?php

abstract class Tea_Hook
{
    abstract public function execute($data = null);
}
