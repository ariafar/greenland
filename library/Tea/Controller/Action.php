<?php

class Tea_Controller_Action extends Zend_Controller_Action
{

    protected $htmlResponse;
    protected $services;

    protected $sendJson = false;
    protected $isAjax   = false;

    protected $serviceManager;

    public function __construct(Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array()
    ) {
        parent::__construct($request, $response, $invokeArgs);
        if ($this->view) {
            $viewsPath = implode(
                DS,
                array(
                    APPLICATION_PATH,
                    'modules',
                    $request->getModuleName(),
                    '/views/scripts/'
                )
            );
            $this->view->setScriptPath($viewsPath);
        }
    }

    public function main($params)
    {
    }

    public function preDispatch()
    {
        $x = $this->_getParam('type', 'html');
        if ($this->_getParam('type', 'html')=='Json') {
            $this->sendJson = true;
        }

        if (!($this->isAjax)) {
            $this->main(array('action'=>$this->_getParam('action')));
        }
    }
    
    public function isAccessible($context, $action, $item = "*", $returnResponse = true)
    {
        $user = Users_Service_Auth::getInstance()->getCurrentUser();
        if ($user == null) {
            $this->getResponse()
                ->setHttpResponseCode(401)
                ->appendBody("Unauthorized");
            return false;
        }

        $acl  = Zend_Registry::getInstance()->get('acl');
        if (!$acl->isAccessible($context, $action, $item)) {
            $this->getResponse()
                ->setHttpResponseCode(403)
                ->appendBody("Forbidden");
            return false;
        }

        return true;
    }
}
