<?php

class Tea_Controller_Router_Cli extends Zend_Controller_Router_Abstract
{

    public function route(Zend_Controller_Request_Abstract $request)
    {

        if (!$request instanceof Tea_Controller_Request_Cli) {
            $error = 'Tea_Controller_Router_Cli requires a' .
                'Tea_Controller_Request_Cli request object';

            throw new Zend_Controller_Router_Exception($error);
        }

        $front = Zend_Controller_Front::getInstance();
        $opts  = new Zend_Console_Getopt(
            array (
                "a|action=s"      =>"Action",
                "c|controller=s"  => "Controller",
                "m|module=s"      => "Module",
                "e|environment=s" => "Application Environment"
            )
        );

        try {
            $opts->parse();
        } catch (Zend_Console_Getopt_Exception $e) {
            throw new Zend_Controller_Router_Exception(
                $e->getMessage() . "\n" . $e->getUsageMessage()
            );
        }

        if (isset($opts->a)) {
            $request->setActionName($opts->a);
        } else {
            $request->setActionName($front->getDefaultAction());
        }

        if (isset($opts->c)) {
            $request->setControllerName($opts->c);
        } else {
            $request->setControllerName($front->getDefaultControllerName());
        }

        if (isset($opts->m)) {
            $request->setModuleName($opts->m);
        } else {
            $request->setModuleName($front->getDefaultModule());
        }

        if (count($opts->getRemainingArgs()) > 0) {
            $request->setParams($opts->getRemainingArgs());
        }
        return $request;
    }

    public function assemble($userParams, $name = null,
        $reset = false, $encode = true
    ) {
        return null;
    }
}
