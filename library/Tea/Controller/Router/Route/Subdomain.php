<?php

class Tea_Controller_Router_Route_Subdomain extends Zend_Controller_Router_Route_Hostname
{
    public function __construct($route, $variable, $defaults = array(), $scheme = null)
    {
        $this->_route    = $route;
        $this->_defaults = (array) $defaults;
        $this->_varName  = (string) $variable;
        $this->_scheme   = $scheme;
    }

    public function match($request)
    {
        // Check the scheme if required
        if ($this->_scheme !== null) {
            $scheme = $request->getScheme();

            if ($scheme !== $this->_scheme) {
                return false;
            }
        }

        // Get the host and remove unnecessary port information
        $host = $request->getHttpHost();
        preg_match('/^(.*)\.[^.]++\.[^.]++$/', $host, $domain);
        if (empty($domain) || $domain[1] == "www" ) {
            $domain = null;
        } else {
            $domain = trim(str_ireplace('www.', '', $domain[1]));
            if (empty($domain)) {
                $domain = null;
            }
        }

        if ($domain == null) {
            return false;
        }
        $this->_values = array(
            $this->_varName => $domain
        );

        return $this->_values + $this->_defaults;
    }
}
