<?php
//namespace Everyman\Neo4j;

/**
 * Represents a single node in the database
 */
class Tea_Neo4j_Node extends Tea_Neo4j_PropertyContainer 
{
	/**
	 * Delete this node
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Tea_Neo4j_Exception on failure
	 */
	public function delete()
	{
		$this->client->deleteNode($this);
		return $this;
	}

	/**
	 * Find paths from this node to the given node
	 *
	 * @param Node $to
	 * @param string $type
	 * @param string $dir
	 * @return Tea_Neo4j_PathFinder
	 */
	public function findPathsTo(Tea_Neo4j_Node $to, $type=null, $dir=null)
	{
		$finder = new Tea_Neo4j_PathFinder($this->client);
		$finder->setStartNode($this);
		$finder->setEndNode($to);
		if ($dir) {
			$finder->setDirection($dir);
		}

		if ($type) {
			$finder->setType($type);
		}

		return $finder;
	}

	/**
	 * Get relationships of this node
	 *
	 * @param mixed  $types string or array of strings
	 * @param string $dir
	 * @return array of Tea_Neo4j_Relationship
	 */
	public function getRelationships($types=array(), $dir=null)
	{
		return $this->client->getNodeRelationships($this, $types, $dir);
	}

	/**
	 * Load this node
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Tea_Neo4j_Exception on failure
	 */
	public function load()
	{
		$this->client->loadNode($this);
		return $this;
	}

	/**
	 * Make a new relationship
	 *
	 * @param Node $to
	 * @param string $type
	 * @return Tea_Neo4j_Relationship
	 */
	public function relateTo(Tea_Neo4j_Node $to, $type)
	{
		$rel = $this->client->makeRelationship();
		$rel->setStartNode($this);
		$rel->setEndNode($to);
		$rel->setType($type);

		return $rel;
	}

	/**
	 * Save this node
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Exception on failure
	 */
	public function save()
	{
		$this->client->saveNode($this);
		return $this;
	}
}
