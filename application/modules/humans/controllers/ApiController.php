<?php

class Humans_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $humans_service = Humans_Service_Human::getInstance();
        $humans = $humans_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($humans as $item) {
            $item = $item->toArray();
            if (isset($item['photoId'])) {
                $r_service = Resources_Service_Resource::getInstance();
                $file_id = $item['photoId'];
                $file_id && $file = $r_service->getByPK($file_id);
                if ($file) {
                    $config = Zend_Registry::get('config');
                    $file && $item['photo'] = $file->toArray();
                    $item["path"] = $config->app->url . '/r/' . $file->getId();
                };
            }
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Humans_Service_Human::getInstance();
        $human = $service->getByPK($id);

        if ($human)
            $result = $human->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $human = new Humans_Model_Human();
        $human->setupdateDate('now');
        $human->fill($params);
        $result = Humans_Service_Human::getInstance()
                ->save($human);

        $item = $result->toArray();

        if (isset($params['photoId'])) {
            $r_service = Resources_Service_Resource::getInstance();
            $file_id = $params['photoId'];
            $file_id && $file = $r_service->getByPK($file_id);
            
            $file->setParentId($item["id"]);
            $file->setNew(false);
            $file = $r_service->save($file);
            if ($file) {
                $config = Zend_Registry::get('config');
                $file && $item['photo'] = $file->toArray();
                $item["path"] = $config->app->url . '/r/' . $file->getId();
            };
        }

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($item));
            return;
        }
    }

    public function putAction()
    {
        $service = Humans_Service_Human::getInstance();
        $params = $this->getParams();

        $human = $service->getByPK($params['id']);
        //TO Do
        if (!$human instanceof Humans_Model_Human) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Human)');
            return;
        }

        $human->fill($params);
        $human->setUpdateDate('now');
        $human->setNew(false);
        $human = $service->save($human);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($human->toArray()));
    }

    public function deleteAction()
    {
        $service = Humans_Service_Human::getInstance();
        $humanId = $this->_getParam('id');
        $human = $service->getByPK($humanId);

        if (!$human instanceof Humans_Model_Human) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Human)");
            return;
        }

        $service->remove($human);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

