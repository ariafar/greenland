<?php

class Humans_Model_Human extends Tea_Model_Entity
{

    const TYPE_CEO = 1;
    const TYPE_SENIOR_MANAGER = 2;

    protected $_properties = array(
        'id' => NULL,
        'name' => NULL,
        'type' => NULL,
        'degree' => NULL,
        'experiences' => NULL,
        'position' => NULL,
        'managerMessage' => NULL,
        'emailAddress' => NULL,
        'telNumber' => NULL,
        'photoId' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'startDate' => NULL,
        'endDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'type' :
                case 'name' :
                case 'degree' :
                case 'experiences' :
                case 'position':
                case 'managerMessage':
                case 'emailAddress':
                case 'photoId':
                case 'telNumber':
                case 'startDate' :
                case 'endDate':
                case 'creationDate' :
                case 'updateDate':
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}