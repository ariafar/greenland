<?php

class Links_Model_Link extends Tea_Model_Entity
{
    const TYPE_SPECIALTY_PAPER_WASTE = 1;
    const TYPE_ENGLISH_PAPER_WASTE = 2;
    const TYPE_EDUCATIONAL_PAPER_WASTE = 3;
    const TYPE_BOOKS_RECYCLED_ART = 4;

    protected $_properties = array(
        'id' => NULL,
        'title' => NULL,
        'description' => NULL,
        'path' => NULL,
        'type' => NULL,
        'fileId' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'title' :
                case 'description' :
                case 'path' :
                case 'type' :
                case 'fileId' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

