<?php

class Markets_Model_Market extends Tea_Model_Entity
{

    const TYPE_MAGAZINE = 0;
    const TYPE_PAPAER = 1;
    const TYPE_RULE = 2;
    const ACTIVITI_PERMANENT = 1;
    const ACTIVITY_NEIGHBORHOOD = 2;

    protected $_properties = array(
        'id' => NULL,
        'parentId' => 0,
        'marketCode' => NULL,
        'name' => NULL,
        'establishDate' => NULL,
        'activityType' => NULL,
        'boothCount' => NULL,
        'conexCount' => NULL,
        'kioskCount' => NULL,
        'placeCount' => NULL,
        'locationCount' => NULL,
        'frankishCount' => NULL,
        'customerCount' => NULL,
        'parking' => NULL,
        'tell' => NULL,
        'fax' => NULL,
        'isActive' => 1,
        'address' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'parentId' :
                case 'marketCode' :
                case 'name':
                case 'establishDate' :
                case 'activityType' :
                case 'boothCount' :
                case 'conexCount' :
                case 'kioskCount':
                case 'placeCount' :
                case 'locationCount' :
                case 'frankishCount' :
                case 'customerCount' :
                case 'parking' :
                case 'tell' :
                case 'fax' :
                case 'isActive' :
                case 'address' :
                case 'latitude' :
                case 'longitude' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

