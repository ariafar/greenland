<?php

class Users_Api_RegionsController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {

        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();

        $params = $this->_getAllParams();
        $userId = $this->_getParam('id');

//        if (!Zend_Auth::getInstance()->hasIdentity()) {
//            $this->getResponse()
//                    ->setHttpResponseCode(401)
//                    ->appendBody("Unauthorized");
//            return;
//        }



        if ($user->getType() == Users_Model_User::TYPE_ADMIN) {
            $regionService = Regions_Service_Region::getInstance();
            $region_lists = $regionService->getList();
        } else {
            $regionService = Regions_Service_UserRegions::getInstance();
            $regions = $regionService->getUserRegions($userId);
            $region_lists = array();

            //TO GET Region Districts
            $regionService = Regions_Service_Region::getInstance();
            foreach ($regions as $region) {
                $region = $region->getProperties();
                $districts = $regionService->getRegionsDistricts($region['regionId']);
                $region['districts'] = $districts;
                $region_lists[] = $region;
            }
        }



        $response = array(
            'count' => $count,
            'list' => $region_lists
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

}
