<?php

class Users_Service_User extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_Users();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Users_Model_User &$user = null)
    {
        if ($ret = $this->staticGet($id)) {
            if ($user instanceof Users_Model_User) {
                $user = $ret;
            }

            return $ret;
        }

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$user instanceof Users_Model_User) {
            $user = new Users_Model_User();
        }
        $user->fill($row);
        $user->setNew(false);

        $this->staticSet($id, $user);
        return $user;
    }

    public function getByPKs($ids)
    {
        if (empty($ids)) {
            return array();
        }

        $rows = $this->_table->fetchAll(
                $this->_table->select()->where('id IN (?)', $ids)
        );

        $result = array();
        foreach ($rows as $row) {
            $user = new Users_Model_User();
            $user->fill($row);
            $user->setNew(false);

            $result[] = $user;
        }

        return $result;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;
        $select->from(array('u' => 'users'), array('*'));
        $cSelect->from(array('u' => 'users'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'name':
                        $select->where("CONCAT(u.first_name, u.last_name) LIKE '%$value%'");
                        $cSelect->where("CONCAT(u.first_name, u.last_name) LIKE '%$value%'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $users = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $user = new Users_Model_User();
            $user->fill($row);
            $users[] = $user;
        }

        return $users;
    }

    public function getByEmail($email)
    {
        $select = $this->_table->select();
        $select->where("email = ?", $email);

        $row = $this->_table->getAdapter()->fetchRow($select);
        if ($row) {
            $user = new Users_Model_User();
            $user->fill($row);
            $user->setNew(false);
            return $user;
        }
        return;
    }

    public function getUserByOauthId($provider, $id)
    {
        $networkService = Users_Service_Network::getInstance();

        $filter = array(
            'oauth_id' => $id,
            'provider' => $provider
        );
        $rows = $networkService->getList($filter, array(), 0, $count, 1);

        if (count($rows) == 0) {
            return null;
        } else {
            $rows = $this->_table->find($rows[0]->getUserId());
            if (count($rows) == 0) {
                return null;
            }
            $row = $rows->current()->toArray();
            $user = new Users_Model_User();

            $user->fill($row);
            $user->setNew(false);
            return $user;
        }
    }

    public function save(Users_Model_User $user)
    {
        if ($user->isNew()) {
            $data = $user->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $this->getByPK($pk, $user);

                $this->staticSet($id, $user);

//                Tea_Hook_Registry::dispatchEvent('join_user', $user);
                return $user;
            }
        } else {
            $id = $user->getId();
            $this->staticDel($id);
            $data = $user->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);

            $this->getByPK($id, $user);
            return $user;
        }

        return false;
    }

    public function saveUserAndProfile(Users_Model_User $user, Users_Model_Profile $profile)
    {
        $profileService = Users_Service_Profile::getInstance();
        $authService = Users_Service_Auth::getInstance();

        $isNew = $user->isNew();

        $this->_table->getAdapter()->beginTransaction();
        try {
            $this->save($user);
            $profileService->save($profile);

            if ($user->getProfileId() == null) {
                $user->setProfileId($profile->getId());
                $user->setNew(false);
                $user = $this->save($user);
            }

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        if (!$isNew) {
            $authService->saveCurrentUser($user, false);
            Tea_Hook_Registry::dispatchEvent('edit_user', $user);
        }

        return true;
    }

    public function remove(Users_Model_User $user)
    {
        $this->staticDel($user->getId());

        $followService = Users_Service_Follow::getInstance();
        $networkService = Users_Service_Network::getInstance();
        $prefService = Users_Service_Pref::getInstance();
        $profileService = Users_Service_Profile::getInstance();
        $userRoleService = Users_Service_UserRole::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------- delete related tables entries ---------
            $followService->removeByUser($user);
            $networkService->removeByUser($user);
            $prefService->removeByUser($user);
            $profileService->removeByUser($user);
            $userRoleService->removeByUser($user);
            //-----------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('id = ?', $user->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeAll()
    {
        $followService = Users_Service_Follow::getInstance();
        $networkService = Users_Service_Network::getInstance();
        $prefService = Users_Service_Pref::getInstance();
        $profileService = Users_Service_Profile::getInstance();
        $userRoleService = Users_Service_UserRole::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------- delete related tables entries ---------
            $followService->removeAll();
            $networkService->removeAll();
            $prefService->removeAll();
            $profileService->removeAll();
            $userRoleService->removeAll();
            //-----------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function getDetails(Users_Model_User $user, $currentUser = null)
    {
        if (!$user) {
            return false;
        }

//        $fuzzy_date = $this->getFuzzyDate($user->getExpireDate());
//        $user->setFuzzyExpireDate($fuzzy_date);

        return $user;
    }

    public function getFuzzyDate($date)
    {
//        $diff = date('Y-m-d H:i:s') - $date;
        $diff = strtotime($date) - strtotime(date('Y-m-d H:i:s'));
        $days = floor($diff / (3600 * 24)) ;
        return $days + 1;
//        return $this->getFuzzyFormat($date);
    }

    public function setExpireDate($user_id, $date)
    {
        $user = $this->getByPK($user_id);
        $user->setExpireDate($date);
        $user->setNew(false);
        $this->save($user, false);
    }

    public function setStatus($user_id, $status)
    {
        $user = $this->getByPK($user_id);
        $user->setStatus($status);
        $user->setNew(false);
        $this->save($user, false);
    }

}
