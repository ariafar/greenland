<?php

class Users_Service_Service extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Users_Model_DbTable_Service();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

     public function getByPK($id, Users_Model_Service $service = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$service instanceof Users_Model_Service) {
            $service = new Users_Model_Service();
        }
        $service->fill($row);
        $service->setNew(false);
        return $service;
    }

    public function getByUserId($userId)
    {
        $select = $this->_table->select();
        $select->where("userId = ?", $userId);

        $row = $this->_table->getAdapter()->fetchRow($select);
        if ($row) {
            $servive_model = new Users_Model_Service();
            $servive_model->fill($row);
            return $servive_model;
        }
        return;
    }

    public function save(Users_Model_Service $service)
    {
        if ($service->isNew()) {
            $data = $service->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $service);
            }
        } else {
            $id = $service->getId();
            $data = $service->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $service);
        }

        return false;
    }

    public function remove(Users_Model_Service $service)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $service->getId());
        $this->_table->delete($where);
    }

}
