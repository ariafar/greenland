<?php

class Educations_Service_Education extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Educations_Model_DbTable_Educations();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Educations_Model_Education $education = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$education instanceof Educations_Model_Education) {
            $education = new Educations_Model_Education();
        }
        $education->fill($row);
        $education->setNew(false);
        return $education;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'educations'), array('*'));
        $cSelect->from(array('u' => 'educations'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $educations = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Educations_Model_Education();
            $item->fill($row);
            $educations[] = $item;
        }

        return $educations;
    }

    public function save(Educations_Model_Education $education)
    {
        if ($education->isNew()) {
            $data = $education->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $education);
            }
        } else {
            $id = $education->getId();
            $data = $education->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $education);
        }

        return false;
    }

    public function remove(Educations_Model_Education $education)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $education->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
