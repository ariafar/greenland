<?php

class Memories_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $memories_service = Memories_Service_Memory::getInstance();
        $memories = $memories_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($memories as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "memory"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Memories_Service_Memory::getInstance();
        $memory = $service->getByPK($id);

        if ($memory)
            $result = $memory->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $memory = new Memories_Model_Memory();
        $memory->setupdateDate('now');
        $memory->fill($params);
        $result = Memories_Service_Memory::getInstance()
                ->save($memory);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Memories_Service_Memory::getInstance();
        $params = $this->getParams();

        $memory = $service->getByPK($params['id']);
        //TO Do
        if (!$memory instanceof Memories_Model_Memory) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Memory)');
            return;
        }

        $memory->fill($params);
        $memory->setUpdateDate('now');
        $memory->setNew(false);
        $memory = $service->save($memory);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($memory->toArray()));
    }

    public function deleteAction()
    {
        $service = Memories_Service_Memory::getInstance();
        $memoryId = $this->_getParam('id');
        $memory = $service->getByPK($memoryId);

        if (!$memory instanceof Memories_Model_Memory) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Memory)");
            return;
        }

        $service->remove($memory);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

