<?php

class Projects_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $projects_service = Projects_Service_Project::getInstance();

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $projects = $projects_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($projects as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "projects"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Projects_Service_Project::getInstance();
        $project = $service->getByPK($id);
        $deletedImage = $this->_getParam('deletedImage');

        if (!$project) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (News)");
            return;
        }

        $result = $project->toArray();
        $r_service = Resources_Service_Resource::getInstance();
        $imageFilter = array(
            "parentId" => $result["id"],
            "parentType" => "projects"
        );
        isset($deletedImage) && $imageFilter["deleted"] = $deletedImage;
        $images = $r_service->getImagesByParent($imageFilter);
        $result["photos"] = $images;

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();
        $images = $params["images"];

        $user = $authService->getCurrentUser();
        if (!isset($user)) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Unauthorized');
            return;
        }

        $project = new Projects_Model_Project();
        $project->setupdateDate('now');
        $project->fill($params);
        $result = Projects_Service_Project::getInstance()
                ->save($project);
        if ($result) {
            $new_item = $result->toArray();

            if (count($images)) {
                foreach ($images as $img) {
                    $r_model = new Resources_Model_Resource();
                    $r_service = Resources_Service_Resource::getInstance();

                    $img['parentId'] = $result->getId();
                    $r_model->fill($img);
                    $r_model->setNew(false);
                    $r_service->save($r_model);
                }

                $new_item["photos"] = $images;
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = Projects_Service_Project::getInstance();
        $params = $this->getParams();

        $project = $service->getByPK($params['id']);
        //TO Do
        if (!$project instanceof Projects_Model_Project) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Project)');
            return;
        }

        $project->fill($params);
        $project->setUpdateDate('now');
        $project->setNew(false);
        $project = $service->save($project);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($project->toArray()));
    }

    public function deleteAction()
    {
        $service = Projects_Service_Project::getInstance();
        $projectId = $this->_getParam('id');
        $project = $service->getByPK($projectId);

        if (!$project instanceof Projects_Model_Project) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Project)");
            return;
        }

        $service->remove($project);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

