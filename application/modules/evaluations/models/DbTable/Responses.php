<?php

class Evaluations_Model_DbTable_Responses extends Zend_Db_Table_Abstract
{
    protected $_name    = 'responses';
    protected $_primary = array('responderId','questionId', 'value');

}

?>
