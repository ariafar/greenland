<?php

class Evaluations_Model_Evaluation extends Tea_Model_Entity
{

    const STATUS_OPENED = 0;
    const STATUS_CLOSED = 1;
    const TYPE_POLL = 1;
    const TYPE_CONTEST = 2;

    protected $_properties = array(
        'id' => null,
        'type' => null,
        'title' => null,
        'status' => null,
        'responseCount' => null,
        'winnerId' => null,
        'winnerName' => null,
        'creationDate' => null,
        'updateDate' => null,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setStatus(self::STATUS_OPENED);
//        $this->setPrivacy(0);
        $this->setResponseCount(0);
        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'type' :
                case 'title' :
                case 'status' :
                case 'responseCount' :
                case 'winnerId':
                case 'winnerName' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function setQuestions($questions = array())
    {
        $this->_questions = $questions;
    }

    public function getQuestions()
    {
        if (!isset($this->_questions)) {
            $this->_questions = array();
        }
        return $this->_questions;
    }

}

?>
