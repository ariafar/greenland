<?php

class Evaluations_Model_Question extends Tea_Model_Entity
{

    const TYPE_CHECKBOX = 1;
    const TYPE_RADIO = 2;

    protected $_properties = array(
        'id' => null,
        'evaluationId' => null,
        'question' => null,
        'type' => null,
        'answer' => null, //index of corect option
        'responseCount' => null,
        'creationDate' => null,
        'updateDate' => null,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setResponseCount(0);
//        $this->setWeight(1);
        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'evaluationId' :
                case 'question' :
                case 'type':
                case 'answer':
                case 'responseCount' :
                case 'creationDate' :
                case 'updateDate':
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function setItems($items = array())
    {
        $this->_items = $items;
    }

    public function getItems()
    {
        if (!isset($this->_items)) {
            $this->_items = array();
        }

        return $this->_items;
    }

}
