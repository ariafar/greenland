<?php

class Galleries_Model_Gallery extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'title' => NULL,
        'description' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'imageCount' => 0,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'title' :
                case 'description' :
                case 'creationDate' :
                case 'updateDate' :
                case 'imageCount' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }


}

