<?php

class Galleries_Service_Gallery extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Galleries_Model_DbTable_Galleries();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Galleries_Model_Gallery $gallery = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$gallery instanceof Galleries_Model_Gallery) {
            $gallery = new Galleries_Model_Gallery();
        }
        $gallery->fill($row);
        $gallery->setNew(false);
        return $gallery;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'galleries'), array('*'));
        $cSelect->from(array('u' => 'galleries'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'title':
                        $select->where("CONCAT(u.title, u.lastName) LIKE '%$value%'");
                        break;
                    
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $galleries = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Galleries_Model_Gallery();
            $item->fill($row);
            $galleries[] = $item;
        }

        return $galleries;
    }

    public function save(Galleries_Model_Gallery $gallery)
    {
        if ($gallery->isNew()) {
            $data = $gallery->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $gallery);
            }
        } else {
            $id = $gallery->getId();
            $data = $gallery->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $gallery);
        }

        return false;
    }

    public function remove(Galleries_Model_Gallery $gallery)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $gallery->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
