<?php

class Resources_Worker_CreateThumbnails
{
    public function work($data)
    {
        $res  = $data['resource'];
        $size = $data['size'];
        $fRes = $res;
        if (is_string($res)) {
            $res = Resources_Service_Resource::getInstance()->getByPK($res);
        }

        if (!is_a($res, 'Resources_Model_Resource')) {
            return false;
        }

        if ($res->getReferenceType() == Resources_Model_Resource::REFERENCE_TYPE_CONTAINER) {
            $array = unserialize($res->getReference());
            $fRes  = Resources_Service_Resource::getInstance()->getByPK($array['origin']);
        } else {
            $fRes = $res;
        }

        $ext  = pathinfo($fRes->getFileName(), PATHINFO_EXTENSION);
        $tmp  = tempnam(
            sys_get_temp_dir(),
            pathinfo($fRes->getFileName(), PATHINFO_FILENAME)
        );
        $sTmp = $tmp . '_' . $size . '.' . $ext;
        $tmp  = $tmp . '.' . $ext;
        $pub  = realpath(APPLICATION_PATH . '/../public' . DIRECTORY_SEPARATOR . $fRes->getReference());
        copy($pub, $tmp);

        /**
         *  Create Thumbnail with phpthumb library
         */
        require_once 'phpthumb/ThumbLib.inc.php';
        list($width, $height) = explode('x', $size);
        $height = (empty($height)) ? 100 : $height;
        $thumb = PhpThumbFactory::create($tmp);
        $thumb->adaptiveResize($width, $height)->save($sTmp);

        /**
         * Save New resource
         */
        $pubPath = realpath(APPLICATION_PATH . '/../public');
        $pInfo   = pathinfo($fRes->getReference());
        $newFile = $pInfo['filename'] . substr(md5(microtime()), 0, 3) . '_' . $size . '.' . $pInfo['extension'];
        $newPath = $pInfo['dirname'] . '/' . $newFile;
        $relPath = $pubPath . DIRECTORY_SEPARATOR . $newPath;

        copy($sTmp, $relPath);

        $nRes = new Resources_Model_Resource();
        $meta  = array();
        $imageInfo = getimagesize($relPath);
        if (is_array($imageInfo)) {
            $meta = array(
                'width'  => $imageInfo[0],
                'height' => $imageInfo[1]
            );
        }
        $nRes->setName(pathinfo($newPath, PATHINFO_FILENAME));
        $nRes->setMetaData($meta);
        $nRes->setFileSize(filesize($relPath));
        $nRes->setFileType(mime_content_type($relPath));
        $nRes->setReferenceType(Resources_Model_Resource::REFERENCE_TYPE_FILE);
        $nRes->setReference($newPath);
        $nRes->setCreatedById($fRes->getCreatedById());
        $nRes->setNew(true);
        $nRes = Resources_Service_Resource::getInstance()->save($nRes);


        if ($res->getReferenceType() == Resources_Model_Resource::REFERENCE_TYPE_CONTAINER) {
            $array = unserialize($res->getReference());
            $array[$size] = $nRes->getId();
            $res->setReference(serialize($array));
            Resources_Service_Resource::getInstance()->save($res);
        } else {
            /**
             *  Save Original resource
             */
            $oRes = clone $fRes;
            $oRes->setNew(true);
            $oRes->setId(null);
            Resources_Service_Resource::getInstance()->save($oRes);

            $resources = array (
                'origin' => $oRes->getId(),
                $size    => $nRes->getId(),
            );
            $res->setReferenceType(Resources_Model_Resource::REFERENCE_TYPE_CONTAINER);
            $res->setReference(serialize($resources));
            $res->setFilePath('');
            $res->setFileSize(0);
            $res->setNew(false);

            Resources_Service_Resource::getInstance()->save($res);
        }

        unlink($tmp);
        unlink($sTmp);
        return $nRes->getId();
    }
}
