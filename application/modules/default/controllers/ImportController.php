<?php

class Default_ImportController extends Tea_Controller_Action
{

    public function init()
    {
        $this->view->moduleLoader()->appendModule('invites');
        $this->view->headLink()->appendStylesheet('css/invites.css');
    }

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $authService = Users_Service_Auth::getInstance();
        if ($authService->getCurrentUser()) {
            $redirect = $this->view->url(
                    array(
                'module' => 'feeds',
                'controller' => 'index'
                    ), 'default'
            );
            $this->_redirect($redirect, array('prependBase' => false));
        }
    }
    
    public function gmailAction()
    {
        require_once 'google/api-client/apiClient.php';

        $config = Zend_Registry::get('config');
        $rUri = $config->oauth->google->redirect_uri;
        $id = $config->oauth->google->client_id;
        $secret = $config->oauth->google->client_secret;
        $devId = $config->oauth->google->developer_id;
        $userSrv = Users_Service_User::getInstance();
        $sugSrv = Users_Service_Suggestion::getInstance();
        $cUser = Users_Service_Auth::getInstance()->getCurrentUser();
        $famSrv = Users_Service_FamiliarPeople::getInstance();
        $session = new Zend_Session_Namespace('google');
        $client = new apiClient();
        $client->setApplicationName('memvoo.com');
        $client->setScopes("http://www.google.com/m8/feeds/");
        $client->setClientId($id);
        $client->setClientSecret($secret);
        $client->setRedirectUri($rUri);
        $client->setDeveloperKey($devId);

        if (isset($session->import_token)) {
            $client->setAccessToken($session->import_token);
        }

        if (!$client->getAccessToken()) {
            $session->redirect = $this->view->url(
                    array(
                'module' => 'default',
                'controller' => 'import',
                'action' => 'gmail'
                    ), 'default'
            );
            $session->token_key = "import_token";
            $authUrl = $client->createAuthUrl();
            $this->_redirect($authUrl);
            return;
        }
        
        try {
            $req = new apiHttpRequest("https://www.google.com/m8/feeds/contacts/default/full");
            $val = $client->getIo()->authenticatedRequest($req);
            $feed = Zend_Gdata::importString($val->getResponseBody());
            
            if (!$cUser instanceof Users_Model_User) {
                $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody("Not Found (User)");
                return;
            }

            $results = array();
            $members = array();
            foreach ($feed as $entry) {
                $xml = simplexml_load_string($entry->getXML());
                $flagIsMember = FALSE;
                $name = (string) $entry->title;
                foreach ($xml->email as $e) {
                    $email = (string) $e['address'];
                    $user = $userSrv->getByEmail($email);
                    if ($user instanceof Users_Model_User) {
                        $sugSrv->addSuggestion(
                            $cUser->getId(), $user->getId(), 'USER'
                        );
                        $user = $userSrv->getDetails($user, $cUser);
                        $members[] = $user;
                        $flagIsMember = TRUE;
                    }
                }

                if ($flagIsMember == FALSE) {
                    //Save on Familiar People
                    $familar = new Users_Model_FamiliarPeople();
                    $familar->setUserId($cUser->getId());
                    $familar->setType(Users_Model_FamiliarPeople::TYPE_GMAIL);
                    $familar->setEmail($email);
                    $familar->setName($name);
                    
                    $filter = array(
                        'user_id' => $familar->getUserId(),
                        'type' => $familar->getType(),
                        'email' => $familar->getEmail()
                    );
                    
                    $findFamilar = $famSrv->getByType($filter);
                    if ($findFamilar == null){
                        $familar->setInvited(0);
                        $famSrv->save($familar);        
                    } else {
                        $familar = $findFamilar;
                    }
                    
                    $results[] = $familar->toArray();
                }
            }
        } catch (Exception $e) {
            $this->view->error = true;
            $this->view->message = $e->getMessage();
            return;
        }

        $url = $this->view->url(
                array(
                    'module' => 'default',
                    'controller' => 'import',
                    'action' => 'contact'
                )
        );
        $session = new Zend_Session_Namespace('import_contacts');
        $session->gmail_contacts = $results;
        $session->gmail_members = $members;
        $this->view->gmail_contacts = $results;
        $this->view->gmail_members = $members;
    }
    
    public function yahooAction()
    {
        set_include_path(
                get_include_path() . PATH_SEPARATOR .
                APPLICATION_PATH . '/../library/OpenID'
        );
        require_once 'OAuth/OAuth.php';
        require_once 'Yahoo/YahooOAuthApplication.class.php';

        $config  = Zend_Registry::get('config');
        $appID   = $config->oauth->yahoo->application_id;
        $consKey = $config->oauth->yahoo->consumer_key;
        $consSec = $config->oauth->yahoo->consumer_secret;
        $domain  = 'http://dev.application.com/import/yahoo';
        $session = new Zend_Session_Namespace('yahoo_oauth');

        $userSrv = Users_Service_User::getInstance();
        $sugSrv = Users_Service_Suggestion::getInstance();
        $cUser  = Users_Service_Auth::getInstance()->getCurrentUser();

        $oauthapp = new YahooOAuthApplication(
                        $consKey,
                        $consSec,
                        $appID,
                        $domain
        );
        
        $famSrv = Users_Service_FamiliarPeople::getInstance();

        if (!$cUser instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }

        if (isset($_GET['openid_mode']) && $_GET['openid_mode'] == 'id_res') {

            $request_token = new YahooOAuthRequestToken($_GET['openid_oauth_request_token'], '');
            $session->request_token = $request_token->to_string();

            // exchange request token for access token
            $oauthapp->token = $oauthapp->getAccessToken($request_token);

            if (!empty($oauthapp->token->oauth_problem)) {
                $this->view->error = $oauthapp->token->oauth_problem;
                $this->view->found = false;
                return;
            }

            // store access token for later use
            $session->access_token = $oauthapp->token->to_string();
            $contacts = $oauthapp->getContacts()->contact;
            $session = new Zend_Session_Namespace('import_contacts');
            $this->view->contacts = $contacts;

            $results = array();
            $members = array();
            foreach ($contacts as $contact) {
                foreach ($contact->fields as $field) {
                    if ($field->type == 'email') {
                        $flagIsMember = FALSE;
                        $email = $field->value;
                        $user = $userSrv->getByEmail($email);
                        if ($user instanceof Users_Model_User) {
                            $sugSrv->addSuggestion(
                                $cUser->getId(), $user->getId(), 'USER'
                            );
                            $user = $userSrv->getDetails($user, $cUser);
                            $members[] = $user;
                            $flagIsMember = TRUE;
                        }
                    }
                }
                
                if ($flagIsMember == FALSE) {
                    //Save on Familiar People
                    $familar = new Users_Model_FamiliarPeople();
                    $familar->setUserId($cUser->getId());
                    $familar->setType(Users_Model_FamiliarPeople::TYPE_YAHOO);
                    $familar->setEmail($email);
                    
                    $filter = array(
                        'user_id' => $familar->getUserId(),
                        'type' => $familar->getType(),
                        'email' => $familar->getEmail()
                    );
                    
                    $findFamilar = $famSrv->getByType($filter);
                    if ($findFamilar == null){
                        $familar->setInvited(0);
                        $famSrv->save($familar);        
                    } else {
                        $familar = $findFamilar;
                    }
                    
                    $results[] = $familar->toArray();
                }
                
            }

            $url = $this->view->url(
                    array(
                        'module'     => 'default',
                        'controller' => 'import',
                        'action'     => 'contact'
                    )
            );
            $session = new Zend_Session_Namespace('import_contacts');
            $session->yahoo_contacts = $results;
            $session->yahoo_members = $members;
            //$this->_redirect($url, array('prependBase' => false));
            return;
        }

        $url = $oauthapp->getOpenIDUrl($oauthapp->callback_url);

        $this->_redirect($url);
    }

    public function facebookAction()
    {
        $userSrv = Users_Service_User::getInstance();
        $cUser  = Users_Service_Auth::getInstance()->getCurrentUser();
        $networkService = Users_Service_Network::getInstance();
        $sugSrv = Users_Service_Suggestion::getInstance();
        $famSrv = Users_Service_FamiliarPeople::getInstance();
        $session = new Zend_Session_Namespace('facebook_oauth');
        
        require_once 'facebook/facebook.php';
        
        if (!$cUser instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }
        if (! isset($session->facebook_id)){
            $config = Zend_Registry::get('config');
            $params = array(
                'redirectUri' => 'http://dev.application.com/import/facebook',//$config->oauth->facebook->redirectUri,
                'callbackUrl' => 'http://dev.application.com/import/facebook',//$config->oauth->facebook->callbackUrl,
                'appId'       => $config->oauth->facebook->appId,
                'secret'      => $config->oauth->facebook->secret,
                'cookie'      => $config->oauth->facebook->cookie
            );
            $facebook = new Facebook($params);
            $uid      = $facebook->getUser();
        } else {
            $uid = $session->facebook_id();
        }
        
        if ($uid) {
            $req = $facebook->api('/me/friends?fields=id,name,picture');
            $friends = $req['data'];
            
            $results = array();
            $members = array();
            foreach ($friends as $friend) {
                $flagIsMember = FALSE;
                $netwrork = $networkService->getByOauthId('facebook', $friend['id']);

                if ($netwrork instanceof Users_Model_Network) {
                    $sugSrv->addSuggestion(
                        $cUser->getId(), $netwrork->getUserId(), 'USER'
                    );
                    $user = $userSrv->getByPK($netwrork->getUserId());
                    if ($user instanceof Users_Model_User) {
                        $sugSrv->addSuggestion(
                            $cUser->getId(), $user->getId(), 'USER'
                        );
                        $user = $userSrv->getDetails($user, $cUser);
                        $members[] = $user;
                        $flagIsMember = TRUE;
                    }
                }
                
                if ($flagIsMember == FALSE) {
                    //Save on Familiar People
                    $familar = new Users_Model_FamiliarPeople();
                    $familar->setUserId($cUser->getId());
                    $familar->setType(Users_Model_FamiliarPeople::TYPE_FACEBOOK);
                    $familar->setNetId($friend['id']);
                    $familar->setName($friend['name']);
                    
                    $filter = array(
                        'user_id' => $familar->getUserId(),
                        'type' => $familar->getType(),
                        'net_id' => $familar->getNetId()
                    );
                    
                    $findFamilar = $famSrv->getByType($filter);
                    if ($findFamilar == null){
                        $familar->setInvited(0);
                        $famSrv->save($familar);        
                    } else {
                        $familar = $findFamilar;
                    }
                    
                    $results[] = $familar->toArray();
                }
            }
            $session = new Zend_Session_Namespace('import_contacts');
            $session->fb_contacts = $results;
            $session->fb_members = $members;
            $this->view->fb_contacts = $results;
            $this->view->fb_members = $members;
        } else {
            $params = array(
                'scope' => 'offline_access,email,read_stream,publish_stream,
                user_location,user_education_history,user_work_history,
                user_birthday,user_relationships'
            );
            $loginUrl = $facebook->getLoginUrl($params);
            $this->_redirect($loginUrl);
            return;
        }
    }
    
    public function twitterAction()
    {   
        $config = Zend_Registry::get('config');
        
        $cUser  = Users_Service_Auth::getInstance()->getCurrentUser();
        $networkService = Users_Service_Network::getInstance();        
        $famSrv = Users_Service_FamiliarPeople::getInstance();
        $consumer = new Zend_Oauth_Consumer($config->oauth->twitter->toArray());
        $session = new Zend_Session_Namespace('twitter_oauth');
        $userSrv = Users_Service_User::getInstance();
        
        if (!isset($session->token)) {
            $session->redirect = $this->view->url(
                array(
                    'module'     => 'default',
                    'controller' => 'import',
                    'action'     => 'twitter'
                ),
                'default'
            );
            $session->requestToken = serialize($consumer->getRequestToken());
            $consumer->redirect();
            return;
        }

        $token   = $session->token;
        $options = array(
            'username'       => $token->screen_name,
            'accessToken'    => $token,
            'consumerKey'    => $config->oauth->twitter->consumerKey,
            'consumerSecret' => $config->oauth->twitter->consumerSecret,
        );
        $twitter = new Zend_Service_Twitter($options);
        $data    = $twitter->account->verifyCredentials();
        $friends = $twitter->userFriends();
        $followers = $twitter->userFollowers();
        
        if (!$cUser instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }
        
        $results = array();
        $members = array();
        foreach ($friends as $friend) {
            $flagIsMember = FALSE;
            $netwrork = $networkService->getByOauthId('twitter', (string)$friend->id);

            if ($netwrork instanceof Users_Model_Network) {
                $sugSrv->addSuggestion(
                    $cUser->getId(), $netwrork->getUserId(), 'USER'
                );
                $user = $userSrv->getByPK($netwrork->getUserId());
                if ($user instanceof Users_Model_User) {
                    $sugSrv->addSuggestion(
                        $cUser->getId(), $user->getId(), 'USER'
                    );
                    $user = $userSrv->getDetails($user, $cUser);
                    $members[] = $user;
                    $flagIsMember = TRUE;
                }
            }                
                
            if ($flagIsMember == FALSE) {
                //Save on Familiar People
                $familar = new Users_Model_FamiliarPeople();
                $familar->setUserId($cUser->getId());
                $familar->setType(Users_Model_FamiliarPeople::TYPE_TWITTER);
                $familar->setNetId((string)$friend->id);
                $familar->setName((string)$friend->name);

                $filter = array(
                    'user_id' => $familar->getUserId(),
                    'type' => $familar->getType(),
                    'net_id' => $familar->getNetId()
                );

                $findFamilar = $famSrv->getByType($filter);
                if ($findFamilar == null){
                    $familar->setInvited(0);
                    $famSrv->save($familar);        
                } else {
                    $familar = $findFamilar;
                }

                $results[] = $familar->toArray();
            }
                
            }
        
//        foreach ($followers as $follower) {
//            $netwrork = $networkService->getByOauthId('twitter', (string)$follower->id);
//
//            if ($netwrork instanceof Users_Model_Network) {
//                $sugSrv->addSuggestion(
//                    $cUser->getId(), $netwrork->getUserId(), 'USER'
//                );
//                $members[] = $user;
//            } else {
//                $res = array(
//                    'id' => (string)$follower->id,
//                    'name' => (string)$follower->name                    
//                );
//                $results[] = $res;
//            }
//        }
        
        $session = new Zend_Session_Namespace('import_contacts');
        $session->twitter_contacts = $results;
        $session->twitter_members = $members;
        $this->view->twitter_contacts = $results;
        $this->view->twitter_members = $members;
    }    
}